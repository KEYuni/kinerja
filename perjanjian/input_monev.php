<?php
include("../inc/pdo.conf.php");

session_start();

$id = 1;

$currentMenu = 'approval';
$path = '../';

$triwulanRomawi = ['', 'I', 'II', 'III', 'IV'];
$bulanTriwulan = ['', 'Januari, Februari, Maret', 'April, Mei, Juni', 'Juli, Agustus, September', 'Oktober, November, Desember'];

$id_perjanjian = isset($_GET["p"]) ? $_GET['p'] : '';
$triwulan = isset($_GET["t"]) ? $_GET['t'] : '';

if ($triwulan) {
    $triwulanR = $triwulanRomawi[$triwulan];
} else {
    $triwulanR = '';
}

$queryPerjanjian = $db->query("SELECT tahun, pihak2 FROM `perjanjian_kinerja` WHERE `id_perjanjian`='$id_perjanjian'");
$data_perjanjian = $queryPerjanjian->fetch(PDO::FETCH_ASSOC);

$queryIndikator = $db->query("SELECT * FROM `indikator_kinerja` WHERE `id_perjanjian`='$id_perjanjian' ORDER BY `id_indikator` ASC");
$dataIndikator = $queryIndikator->fetchAll(PDO::FETCH_ASSOC);

if ($data_perjanjian['pihak2'] != $_SESSION['id_pegawai']) {
    header("location:index2.php");
}

// echo '<pre>';
// print_r($data_perjanjian);
// echo '</pre>';
// exit();
?>

<!DOCTYPE html>
<html lang="en">

<?php
include($path . "template/header.php");
?>

<body>

    <!-- Top Bar Start -->
    <?php
    include($path . "template/topbar.php");
    ?>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php
        include($path . "template/sidenav.php");
        ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="index2.php">Perjanjian Kinerja</a></li>
                                    <li class="breadcrumb-item active">Monev</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Monitoring dan Evaluasi Individu Triwulan <?php echo $triwulanR ?> Tahun <?php echo $data_perjanjian['tahun'] ?></h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="d-flex" style="flex-direction: column;">
                                        <h4 class="mt-0 header-title">Input Monev Kinerja Triwulan <?php echo $triwulanR ?></h4>
                                        <p class="text-muted mb-3">Input Monev Kinerja selama 3 bulan (<?php echo $bulanTriwulan[$triwulan] ?>)
                                        </p>
                                    </div>
                                    <!-- <div style="margin-right: 0px; margin-left:auto">
                                        <button class="btn btn-warning btn-sm" style="height: 35px;"><i class="fas fa-edit"></i> Edit hasil monev</button>
                                        <a href="cetak_monev.php" target="_blank">
                                            <button class="btn btn-primary btn-sm" style="height: 35px;"><i class="fas fa-print"></i> Cetak hasil monev</button>
                                        </a>
                                    </div> -->
                                </div>


                                <form class="form-parsley" action="proses_monev.php" id="form-perjanjian" method="post">
                                    <input type="text" name="triwulan" id="triwulan" value="<?php echo $triwulan ?>" hidden>
                                    <input type="text" name="id_perjanjian" id="id_perjanjian" value="<?php echo $id_perjanjian ?>" hidden>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered mb-0">
                                                    <thead class="thead-light">
                                                        <tr class="text-center">
                                                            <th width="5%">No</th>
                                                            <!-- <th width="20%">Sasaran</th> -->
                                                            <th width="25%">Indikator</th>
                                                            <th width="10%">Satuan</th>
                                                            <th width="10%">Target</th>
                                                            <th width="10%">Realisasi <span class="text-danger">*</span></th>
                                                            <!-- <th width="10%">%</th> -->
                                                            <!-- <th width="10%" class="text-center">Aksi</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <input type="hidden" name="jum_indikator" id="jum_indikator" value="<?php echo count($dataIndikator) ?>">
                                                        <?php foreach ($dataIndikator as $i => $indikator) {
                                                            $target = $indikator['target_' . $triwulan];
                                                            $realisasi = $indikator['realisasi_' . $triwulan];

                                                            if ($realisasi) {
                                                                $persen = round((($realisasi / $target) * 100), 2);
                                                            } else {
                                                                $persen = '';
                                                            }
                                                        ?>

                                                            <tr class="text-center">
                                                                <td><?php echo ($i + 1) ?></td>
                                                                <!-- <td rowspan="3">Meningkatnya Kualitas Lingkungan Sehat, Budaya Sehat, dan Mutu Pelayanan Kesehatan</span></td> -->
                                                                <td><?php echo $indikator['indikator'] ?></td>
                                                                <td><?php echo ucfirst($indikator['satuan']) ?></td>
                                                                <td><?php echo $target; ?></td>
                                                                <td>
                                                                    <?php if ($realisasi) {
                                                                        $valRealisasi = $realisasi;
                                                                    } else {
                                                                        $valRealisasi = '';
                                                                    } ?>
                                                                    <input type="text" name="id_indikator[]" id="id_indikator<?php echo $indikator['id_indikator'] ?>" value="<?php echo $indikator['id_indikator'] ?>" hidden>
                                                                    <input type="text" name="target[]" id="target<?php echo $indikator['id_indikator'] ?>" value="<?php echo $target ?>" hidden>
                                                                    <input class="form-control" type="text" name="realisasi[]" id="realisasi<?php echo $indikator['id_indikator'] ?>" value="<?php echo $valRealisasi ?>" required style="text-align: center;" placeholder="Masukkan realisasi">
                                                                </td>
                                                                <!-- <td></td> -->
                                                                <!-- <td>
                                                                <button class="btn btn-xs btn-success btn-block" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" style="height: max-content; margin-right:0px; margin-left:auto">Input realisasi</button>
                                                            </td> -->
                                                            </tr>

                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <a href="index2.php">
                                                <button class="btn btn-danger btn-sm float-left mt-4" type="button" style="height: 35px; width:80px">Kembali</button>
                                            </a>
                                            <button class="btn btn-success mt-4 btn-sm" type="button" id="btn-save-monev">Simpan hasil monev</button>
                                        </div>
                                    </div>
                                </form>
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div><!-- container -->

            <footer class="footer text-center text-sm-left">
                &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery -->
    <?php
    include($path . "template/footer.php");
    ?>

    <script type="text/javascript">
        $(function() {
            //A title with a text under
            $('#btn-save-monev').click(function() {

                jum_indikator = $("#jum_indikator").val();
                id_perjanjian = $("#id_perjanjian").val();
                triwulan = $("#triwulan").val();

                console.log(jum_indikator)

                var id_indikator = $("input[name='id_indikator[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var target = $("input[name='target[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var value_realisasi = $("input[name='realisasi[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var realisasi = value_realisasi.filter(function(el) {
                    return el != '';
                });

                if (realisasi.length == jum_indikator) {

                    id_indikator = JSON.stringify(id_indikator)
                    target = JSON.stringify(target)
                    realisasi = JSON.stringify(realisasi)

                    var fd = new FormData();
                    fd.append("id_perjanjian", id_perjanjian);
                    fd.append("triwulan", triwulan);
                    fd.append("id_indikator", id_indikator);
                    fd.append("target", target);
                    fd.append("realisasi", realisasi);

                    $.ajax({
                        type: 'POST',
                        url: 'ajax_data/proses_monev.php',
                        data: fd,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(response) {
                            console.log(response);

                            swal.fire({
                                title: "Monev Perjanjian Kinerja",
                                text: "Hasil Monev Perjanjian kinerja berhasil disimpan.",
                                type: 'success',
                            }).then((result) => {
                                window.location = "index2.php";
                            })
                        }
                    });

                } else {

                    swal.fire({
                        // title: "Monev Perjanjian Kinerja",
                        text: "Masukkan realisasi semua indikator.",
                        type: 'warning',
                    })
                }

            });

            $(".select2").select2({
                width: '100%'
            });
        });

        function delFormIndokator(no) {
            $("#formIndikator" + no).remove();
            $("#hrFormIndikator" + no).remove();
            // $("#val_varian" + id).remove();
            // console.log('delBadge')
            console.log(no)
        }
    </script>
</body>

</html>