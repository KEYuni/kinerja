<?php
include("../inc/pdo.conf.php");

session_start();
$currentMenu = 'perjanjian';
$path = '../';

$dataPegawai = array(
    [
        'id_pegawai' => '2',
        'nama' => 'Iwang Suwangsih, SE',
        'jabatan' => 'PLT. Kepala Sub. Bagian Perencanaan dan Anggaran',
        'nik' => '198004282007012018',
        'golongan' => 'Penata Muda Tk.I',
    ],
    [
        'id_pegawai' => '3',
        'nama' => 'Iwan Setiawan',
        'jabatan' => 'PLT. Kepala Sub. Bagian Tata Usaha',
        'nik' => '196509291988031008',
        'golongan' => 'Penata Tk.I, III/d',
    ],
);

// echo '<pre>';
// print_r($pegawai);
// echo '</pre>';

// $idArray = array_column($pegawai, 'id_pegawai');
// $indexSearch = array_search('3', $idArray);
// echo '<pre>';
// print_r($idArray);
// echo '</pre>';
// echo '<pre>';
// print_r($indexSearch);
// echo '</pre>';
// exit();
?>
<!DOCTYPE html>
<html lang="en">

<?php
include($path . "template/header.php");
?>

<body>

    <!-- Top Bar Start -->
    <?php
    include($path . "template/topbar.php");
    ?>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php
        include($path . "template/sidenav.php");
        ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="index.php">Perjanjian Kinerja</a></li>
                                    <li class="breadcrumb-item active">Buat</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Buat Perjanjian Kinerja</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">Input Indikator Kinerja</h4>
                                <p class="text-muted mb-3">Input Indikator Kinerja selama satu tahun kerja
                                </p>

                                <form class="form-parsley" action="save_perjanjian.php" method="post" id="form-perjanjian">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label text-left">Tahun perjanjian</label>
                                                <div class="col-sm-10">
                                                    <select class="select2 form-control" name="tahun" id="tahun">
                                                        <option>-- Pilih tahun perjanjian dibuat --</option>
                                                        <option>2020</option>
                                                        <option>2021</option>
                                                        <option>2022</option>
                                                        <option>2023</option>
                                                        <option>2024</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label text-left">Pihak kedua perjanjian</label>
                                                <div class="col-sm-10">
                                                    <select class="select2 form-control" id="pihak2" name="pihak2">
                                                        <option>-- Pilih pihak kedua perjanjian kinerja --</option>
                                                        <?php foreach ($dataPegawai as $i => $pegawai) { ?>
                                                            <option value="<?php echo $pegawai['id_pegawai'] ?>"><?php echo $pegawai['nama'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <input type="text" id="noFormIndikator" name="noFormIndikator" value="0" hidden>
                                    <div class="row px-2" id="formIndikator0" style="margin-right: 5px;">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Indikator Kinerja</label>
                                                <textarea required class="form-control" rows="6" id="indikator0" name="indikator[]"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <div class="row">
                                                        <div class="col-md-12 mb-1">
                                                            <div class="form-group">
                                                                <label>Satuan</label>
                                                                <input type="text" class="form-control" id="satuan0" name="satuan[]" required placeholder="Masukan satuan output dari indikator. Contoh: Laporan." />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Target triwulan 1</label>
                                                                <input type="number" class="form-control" id="triwulan01" name="triwulan1[]" min="1" required />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Target triwulan 2</label>
                                                                <input type="number" class="form-control" id="triwulan02" name="triwulan2[]" min="1" required />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Target triwulan 3</label>
                                                                <input type="number" class="form-control" id="triwulan03" name="triwulan3[]" min="1" required />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Target triwulan 4</label>
                                                                <input type="number" class="form-control" id="triwulan04" name="triwulan4[]" min="1" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-danger h-100" onclick="delFormIndokator(0)"><i class="fas fa-trash-alt font-16"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr id="hrFormIndikator0">
                                    <div id="divFormIndikator">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="button" class="btn btn-sm btn-success" id="addFormIndikator">Tambah Indikator Kinerja</button>
                                        </div>
                                    </div>
                                    <!--end form-group-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                    Submit
                                                </button>
                                                <a href="<?php echo $path ?>index.php">
                                                    <button type="button" class="btn btn-danger waves-effect m-l-5">
                                                        Cancel
                                                    </button>
                                                </a>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                    </div>
                                </form>
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div><!-- container -->

            <footer class="footer text-center text-sm-left">
                &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php
    include($path . "template/footer.php");
    ?>

    <script type="text/javascript">
        $(function() {
            //A title with a text under
            $('#btnAddPerjanjian').click(function() {

                tahun = $("#tahun").val();
                pihak2 = $("#pihak2").val();

                var indikator = [];
                $("textarea[name='indikator[]']").each(function() {
                    var text = $(this).val();
                    console.log($(this))
                    indikator.push(text);
                });

                var satuan = $("input[name='satuan[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan1 = $("input[name='triwulan1[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan2 = $("input[name='triwulan2[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan3 = $("input[name='triwulan3[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan4 = $("input[name='triwulan4[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();
                cekIndikator = indikator.includes('');
                cekSatuan = satuan.includes('');
                cekTriwulan1 = triwulan1.includes('');
                cekTriwulan2 = triwulan2.includes('');
                cekTriwulan3 = triwulan3.includes('');
                cekTriwulan4 = triwulan4.includes('');

                console.log(indikator)
                console.log(satuan)
                console.log(triwulan1)
                console.log(triwulan2)
                console.log(triwulan3)
                console.log(triwulan4)
                console.log('cekIndikator')
                console.log(cekIndikator)
                // console.log(tahun)
                // console.log(pihak2)
                // console.log(indikator)
                // console.log(satuan)
                // console.log(triwulan1)
                // console.log(triwulan2)
                // console.log(triwulan3)
                // console.log(triwulan4)
                // Swal.fire(
                //     'Berhasil!',
                //     'Perjanjian kinerja berhasil dibuat.',
                //     'success'
                // )
                dataIndikator = JSON.stringify(indikator)
                dataTriwulan1 = JSON.stringify(triwulan1)
                dataTriwulan2 = JSON.stringify(triwulan2)
                dataTriwulan3 = JSON.stringify(triwulan3)
                dataTriwulan4 = JSON.stringify(triwulan4)

                var fd = new FormData();
                fd.append("tahun", tahun);
                fd.append("pihak2", pihak2);
                fd.append("indikator", dataIndikator);
                fd.append("triwulan1", dataTriwulan1);
                fd.append("triwulan2", dataTriwulan2);
                fd.append("triwulan3", dataTriwulan3);
                fd.append("triwulan4", dataTriwulan4);

                $.ajax({
                    type: 'POST',
                    url: 'ajax_data/save_perjanjian.php',
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(response) {
                        // var data = JSON.parse(response);
                        console.log(response);

                        // if (active == 4 && no == 5) {
                        // swal({
                        //     title: "Surat Pengantar Tindakan/Operasi",
                        //     text: "Data Surat Pengantar berhasil disimpan.",
                        //     icon: "success",
                        //     button: "Tutup",
                        // }).then((value) => {
                        //     window.location = "form_pra_anestesi.php?id=" + id_periksa +
                        //         "&reg=" + id_register +
                        //         "&poli=anestesi";
                        // });
                        // }
                        // pindah_halaman(data.id, data.kode);
                    }
                });
            });
            $('#addFormIndikator').click(function() {
                no = $("#noFormIndikator").val();
                no = parseInt(no) + 1
                $("#noFormIndikator").val(no);
                console.log(no)

                html = '<div class="row px-2" id="formIndikator' + no + '" style="margin-right: 5px;">' +
                    '<div class="col-md-5">' +
                    '<div class="form-group">' +
                    '<label>Indikator Kinerja</label>' +
                    '<textarea required class="form-control" rows="6" id="indikator' + no + '" name="indikator[]"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-7">' +
                    '<div class="row">' +
                    '<div class="col-md-11">' +
                    '<div class="row">' +
                    '<div class="col-md-12 mb-1">' +
                    '<div class="form-group">' +
                    '<label>Satuan</label>' +
                    '<input type="text" class="form-control" id="satuan' + no + '" name="satuan[]" required placeholder="Masukan satuan output dari indikator. Contoh: Laporan." />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 1</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '1" name="triwulan1[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 2</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '2" name="triwulan2[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 3</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '3" name="triwulan3[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 4</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '4" name="triwulan4[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-1">' +
                    '<button type="button" class="btn btn-danger h-100" onclick="delFormIndokator(' + no + ')"><i class="fas fa-trash-alt font-16"></i></button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div><hr id="hrFormIndikator' + no + '">'

                const element = document.getElementById("divFormIndikator");
                var container = document.createElement("div");
                container.innerHTML = html;
                element.appendChild(container);
                // console.log(html)

            });

            $(".select2").select2({
                width: '100%'
            });
        });

        function delFormIndokator(no) {
            $("#formIndikator" + no).remove();
            $("#hrFormIndikator" + no).remove();
            // $("#val_varian" + id).remove();
            // console.log('delBadge')
            console.log(no)
        }
    </script>
</body>

</html>