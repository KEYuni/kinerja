<?php
include("../../inc/pdo.conf.php");
session_start();

$id_perjanjian = isset($_POST['id']) ? $_POST['id'] : '';

$kosong = array(
    'id_monev' => 0,
    'id_perjanjian' => $id_perjanjian,
    'persentase' => '',
    'dokumen' => '',
    'status' => 'belum dimonev',
    'created_at' => '',
    'updated_at' => '',
);

for ($i = 1; $i < 5; $i++) {
    $qMonev = $db->query("SELECT m.* FROM `perjanjian_kinerja` as pk LEFT JOIN `monev` as m ON pk.id_perjanjian = m.id_perjanjian WHERE pk.`id_perjanjian`='$id_perjanjian' AND m.triwulan='$i'");
    $dataMonev = $qMonev->fetch(PDO::FETCH_ASSOC);
    if ($dataMonev) {
        $data[$i] = $dataMonev;
    } else {
        $kosong['triwulan'] = $i;
        $data[$i] = $kosong;
    }
}

echo json_encode($data);
exit();

// for ($i = 0; $i < count($dataIndikator); $i++) {
//     $idPK = $dataIndikator[$i]['id_perjanjian'];
//     $qDelIndikator = $db->query("DELETE FROM indikator_kinerja WHERE `id_perjanjian`='$idPK'");
//     $qDelIndikator->execute();
// }

// $qDelPerjanjian = $db->query("DELETE FROM perjanjian_kinerja WHERE `id_perjanjian`='$id_perjanjian'");
// $qDelPerjanjian->execute();

// echo json_encode($dataIndikator);
// exit();
