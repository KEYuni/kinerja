<?php
include("../../inc/pdo.conf.php");
session_start();

$pihak1 = isset($_SESSION['id_pegawai']) ? $_SESSION['id_pegawai'] : '';
$id_perjanjian = isset($_POST['id_perjanjian']) ? $_POST['id_perjanjian'] : '';
$aksi = isset($_POST['aksi']) ? $_POST['aksi'] : '';
$tahun = isset($_POST['tahun']) ? $_POST['tahun'] : '';

$qpk = $db->query("SELECT * FROM perjanjian_kinerja WHERE pihak1='" . $pihak1 . "' AND tahun='" . $tahun . "' AND status!='ditolak'");
$cekDataPerjanjian = $qpk->rowCount();

// jika surat pengantar tindakan/operasi sudah ada
if ($cekDataPerjanjian > 0) {
    $dataPerjanjian = $qpk->fetch(PDO::FETCH_ASSOC);

    $result = array(
        'isExist' => 1,
        'status' => $dataPerjanjian['status']
    );
} else {

    $result = array(
        'isExist' => 0,
        'status' => ''
    );
}

echo json_encode($result);
exit();
