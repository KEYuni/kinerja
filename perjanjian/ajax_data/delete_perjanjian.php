<?php
include("../../inc/pdo.conf.php");
session_start();
// include("../../inc/version.php");
// date_default_timezone_set("Asia/Jakarta");
// $namauser = $_SESSION['namauser'];
// $password = $_SESSION['password'];
// $tipe = $_SESSION['tipe'];
// $id_pegawai = $_SESSION['id_pegawai'];
// $tipes = explode('-', $tipe);
// if ($tipes[0] != 'Dokter_module') {
//     unset($_SESSION['tipe']);
//     unset($_SESSION['namauser']);
//     unset($_SESSION['password']);
//     header("location:../../index.php?status=2");
//     exit;
// }
// include "../../inc/anggota_check.php";
$dataPihak2 = array(
    // 'id_pegawai' => '2',
    // 'nama' => 'Iwang Suwangsih, SE',
    // 'jabatan' => 'PLT. Kepala Sub. Bagian Perencanaan dan Anggaran',
    // 'nik' => '198004282007012018',
    // 'golongan' => 'Penata Muda Tk.I'
    'id_pegawai' => '3',
    'nama' => 'Iwan Setiawan',
    'jabatan' => 'PLT. Kepala Sub. Bagian Tata Usaha',
    'nik' => '196509291988031008',
    'golongan' => 'Penata Tk.I, III/d',
);


$id_perjanjian = isset($_POST['id']) ? $_POST['id'] : '';

$qIndikator = $db->query("SELECT * FROM `indikator_kinerja` WHERE `id_perjanjian`='$id_perjanjian' ORDER BY `id_indikator` ASC");
$dataIndikator = $qIndikator->fetchAll(PDO::FETCH_ASSOC);

for ($i = 0; $i < count($dataIndikator); $i++) {
    $idPK = $dataIndikator[$i]['id_perjanjian'];
    $qDelIndikator = $db->query("DELETE FROM indikator_kinerja WHERE `id_perjanjian`='$idPK'");
    $qDelIndikator->execute();
}

$qDelPerjanjian = $db->query("DELETE FROM perjanjian_kinerja WHERE `id_perjanjian`='$id_perjanjian'");
$qDelPerjanjian->execute();

echo json_encode($qDelPerjanjian);
exit();
