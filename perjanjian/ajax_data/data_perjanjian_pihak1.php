<?php
include("../../inc/pdo.conf.php");
session_start();

$table = 'perjanjian_kinerja';
$primaryKey = 'id_perjanjian';
$id = $_SESSION["id_pegawai"];

// $queryPerjanjian = $db->query("SELECT * FROM `perjanjian_kinerja` WHERE `pihak1`='$id' ORDER BY `tahun` DESC");
// $data_perjanjian = $queryPerjanjian->fetchAll(PDO::FETCH_ASSOC);

$columns = array(
    array('db' => 'id_perjanjian', 'dt' => 'id_perjanjian'),
    array('db' => 'pihak1', 'dt' => 'pihak1'),
    array('db' => 'nama_pihak1', 'dt' => 'nama_pihak1'),
    array('db' => 'jabatan_pihak1', 'dt' => 'jabatan_pihak1'),
    array('db' => 'nik_pihak1', 'dt' => 'nik_pihak1'),
    array('db' => 'golongan_pihak1', 'dt' => 'golongan_pihak1'),
    array('db' => 'pihak2', 'dt' => 'pihak2'),
    array('db' => 'nama_pihak2', 'dt' => 'nama_pihak2'),
    array('db' => 'jabatan_pihak2', 'dt' => 'jabatan_pihak2'),
    array('db' => 'nik_pihak2', 'dt' => 'nik_pihak2'),
    array('db' => 'golongan_pihak2', 'dt' => 'golongan_pihak2'),
    array('db' => 'tahun', 'dt' => 'tahun'),
    array('db' => 'status', 'dt' => 'status'),
    array('db' => 'alasan_tolak', 'dt' => 'alasan_tolak'),
    array('db' => 'ket_revisi', 'dt' => 'ket_revisi'),
    array('db' => 'dokumen', 'dt' => 'dokumen'),
    array('db' => 'created_at', 'dt' => 'created_at'),
    array('db' => 'updated_at', 'dt' => 'updated_at'),
);

// SQL server connection information
require_once('../../inc/env_db.php');
$sql_details = array(
    'user' => $userPdo,
    'pass' => $passPdo,
    'db'   => $dbPdo,
    'host' => $hostPdo
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require('../../ssp.customized.class.php');
// $joinQuery = " FROM (SELECT id_permintaan_obat,COUNT(*) as total_data FROM `permintaan_obat_detail` GROUP BY id_permintaan_obat) AS `a` RIGHT JOIN `permintaan_obat` AS `p` ON(`a`.`id_permintaan_obat`=`p`.`id_permintaan_obat`)";
// $extraWhere = " `p`.`asal_ruangan`='" . $nama_ruangan . "' AND `p`.`id_register`='" . $id_register . "'";
$joinQuery = "";
$extraWhere = "";
$groupBy = "";
$having = "";

echo json_encode(
    SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
);
