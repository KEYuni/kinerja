<?php
include("../inc/pdo.conf.php");

session_start();

$id = 1;

$pihak = $_SESSION['pihak'];

if ($pihak == 1) {
    $currentMenu = 'perjanjian';
    $mainPage = 'index.php';
} else {
    $currentMenu = 'approval';
    $mainPage = 'index2.php';
}
$path = '../';

$triwulanRomawi = ['', 'I', 'II', 'III'];
$bulanTriwulan = ['', 'Januari, Februari, Maret', 'April, Mei, Juni', 'Juli, Agustus, September', 'Oktober, November, Desember'];

$id_monev = isset($_GET["m"]) ? $_GET['m'] : '';
$triwulan = isset($_GET["t"]) ? $_GET['t'] : '';

if ($triwulan) {
    $triwulanR = $triwulanRomawi[$triwulan];
} else {
    $triwulanR = '';
}

$queryMonev = $db->query("SELECT id_monev, id_perjanjian FROM `monev` WHERE `id_monev`='$id_monev'");
$data_monev = $queryMonev->fetch(PDO::FETCH_ASSOC);
$id_perjanjian = $data_monev['id_perjanjian'];

// echo '<pre>';
// print_r($data_monev);
// echo '</pre>';
// exit();
$queryPerjanjian = $db->query("SELECT * FROM `perjanjian_kinerja` WHERE `id_perjanjian`='$id_perjanjian'");
$data_perjanjian = $queryPerjanjian->fetch(PDO::FETCH_ASSOC);

$queryIndikator = $db->query("SELECT * FROM `indikator_kinerja` WHERE `id_perjanjian`='$id_perjanjian' ORDER BY `id_indikator` ASC");
$dataIndikator = $queryIndikator->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">

<?php
include($path . "template/header.php");
?>

<body>

    <!-- Top Bar Start -->
    <?php
    include($path . "template/topbar.php");
    ?>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php
        include($path . "template/sidenav.php");
        ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo $mainPage; ?>">Perjanjian Kinerja</a></li>
                                    <li class="breadcrumb-item active">Detail</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Monitoring dan Evaluasi Individu Triwulan <?php echo $triwulanR ?> Tahun <?php echo $data_perjanjian['tahun'] ?></h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="d-flex" style="flex-direction: column;">
                                        <h4 class="mt-0 header-title">Detail Monev Kinerja Triwulan <?php echo $triwulanR ?></h4>
                                        <p class="text-muted mb-3">Detail Monev Kinerja selama 3 bulan (<?php echo $bulanTriwulan[$triwulan] ?>)
                                        </p>
                                    </div>
                                    <?php if ($pihak == 2) { ?>
                                        <div style="margin-right: 0px; margin-left:auto">
                                            <a href="input_monev.php?p=<?php echo $id_perjanjian ?>&t=<?php echo $triwulan ?>">
                                                <button class="btn btn-warning btn-sm" style="height: 35px;"><i class="fas fa-edit"></i> Edit hasil monev</button>
                                            </a>
                                            <a href="cetak_monev.php?m=<?php echo $id_monev ?>&t=<?php echo $triwulan ?>" target="_blank">
                                                <button class="btn btn-primary btn-sm" style="height: 35px;"><i class="fas fa-print"></i> Cetak hasil monev</button>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>


                                <form class="form-parsley" action="proses_monev.php" id="form-perjanjian" method="post">
                                    <input type="text" name="triwulan" id="triwulan" value="<?php echo $triwulan ?>" hidden>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card" style="border: solid 1px #dedede !important">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="d-flex" style="flex-direction: column; margin-right:15px">
                                                            <h6>Nama Staf yang Melaporkan Capaian Kinerja</h6>
                                                            <h6>Jabatan Pelapor Capaian Kinerja</h6>
                                                            <h6>Nama Pimpinan yang Melakukan Monev</h6>
                                                            <h6>Jabatan Pimpinan Pelapor</h6>
                                                        </div>
                                                        <div class="d-flex" style="flex-direction: column;">
                                                            <h6>: <?php echo $data_perjanjian['nama_pihak1'] ?></h6>
                                                            <h6>: <?php echo $data_perjanjian['jabatan_pihak1'] ?></h6>
                                                            <h6>: <?php echo $data_perjanjian['nama_pihak2'] ?></h6>
                                                            <h6>: <?php echo $data_perjanjian['jabatan_pihak2'] ?></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                        </div>
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered mb-0">
                                                    <thead class="thead-light">
                                                        <tr class="text-center">
                                                            <th width="5%">No</th>
                                                            <!-- <th width="20%">Sasaran</th> -->
                                                            <th width="25%">Indikator</th>
                                                            <th width="10%">Satuan</th>
                                                            <th width="10%">Target</th>
                                                            <th width="10%">Realisasi</th>
                                                            <th width="10%">%</th>
                                                            <!-- <th width="10%" class="text-center">Aksi</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $rata2 = 0;
                                                        foreach ($dataIndikator as $i => $indikator) {
                                                            $target = $indikator['target_' . $triwulan];
                                                            $realisasi = $indikator['realisasi_' . $triwulan];
                                                            $persen = round((($realisasi / $target) * 100), 2);
                                                            $rata2 += $persen ?>

                                                            <tr class="text-center">
                                                                <td><?php echo ($i + 1) ?></td>
                                                                <!-- <td rowspan="3">Meningkatnya Kualitas Lingkungan Sehat, Budaya Sehat, dan Mutu Pelayanan Kesehatan</span></td> -->
                                                                <td><?php echo $indikator['indikator'] ?></td>
                                                                <td><?php echo ucfirst($indikator['satuan']) ?></td>
                                                                <td><?php echo $target; ?></td>
                                                                <td><?php echo $realisasi ?></td>
                                                                <td><?php echo  $persen . ' %'  ?></td>
                                                                <!-- <td>
                                                                <button class="btn btn-xs btn-success btn-block" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" style="height: max-content; margin-right:0px; margin-left:auto">Input realisasi</button>
                                                            </td> -->
                                                            </tr>

                                                        <?php }

                                                        $rata2 = $rata2 / count($dataIndikator);
                                                        $rata2 = round($rata2, 2);
                                                        ?>
                                                        <tr class="text-center">
                                                            <td colspan="5">Rata-rata seluruh capaian</td>
                                                            <td><?php echo $rata2 . ' %'; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-12 text-right">
                                            <button class="btn btn-success mt-3 btn-sm" type="submit">Simpan hasil monev</button>
                                        </div> -->
                                    </div>
                                </form>

                                <div class="mt-4">
                                    <a href="<?php echo $mainPage; ?>">
                                        <button class="btn btn-danger btn-sm" style="height: 35px; width:80px">Kembali</button>
                                    </a>
                                </div>
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div><!-- container -->

            <footer class="footer text-center text-sm-left">
                &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery -->
    <?php
    include($path . "template/footer.php");
    ?>

    <script type="text/javascript">
        $(function() {
            //A title with a text under
            $('#btnAddPerjanjian').click(function() {

                tahun = $("#tahun").val();
                pihak2 = $("#pihak2").val();

                var indikator = [];
                $("textarea[name='indikator[]']").each(function() {
                    var text = $(this).val();
                    console.log($(this))
                    indikator.push(text);
                });

                var satuan = $("input[name='satuan[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan1 = $("input[name='triwulan1[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan2 = $("input[name='triwulan2[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan3 = $("input[name='triwulan3[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan4 = $("input[name='triwulan4[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();
                // var names = [];
                // var textarea = document.querySelector('textarea#indikator');

                // function saveNames() {
                //     names = textarea.value.split('\n');
                // }

                cekIndikator = indikator.includes('');
                cekSatuan = satuan.includes('');
                cekTriwulan1 = triwulan1.includes('');
                cekTriwulan2 = triwulan2.includes('');
                cekTriwulan3 = triwulan3.includes('');
                cekTriwulan4 = triwulan4.includes('');

                console.log(indikator)
                console.log('cekIndikator')
                console.log(cekIndikator)
                // console.log(tahun)
                // console.log(pihak2)
                // console.log(indikator)
                // console.log(satuan)
                // console.log(triwulan1)
                // console.log(triwulan2)
                // console.log(triwulan3)
                // console.log(triwulan4)
                // Swal.fire(
                //     'Berhasil!',
                //     'Perjanjian kinerja berhasil dibuat.',
                //     'success'
                // )
            });
            $('#addFormIndikator').click(function() {
                no = $("#noFormIndikator").val();
                no = parseInt(no) + 1
                $("#noFormIndikator").val(no);
                console.log(no)

                html = '<div class="row px-2" id="formIndikator' + no + '" style="margin-right: 5px;">' +
                    '<div class="col-md-5">' +
                    '<div class="form-group">' +
                    '<label>Indikator Kinerja</label>' +
                    '<textarea required class="form-control" rows="6" id="indikator' + no + '" name="indikator[]"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-7">' +
                    '<div class="row">' +
                    '<div class="col-md-11">' +
                    '<div class="row">' +
                    '<div class="col-md-12 mb-1">' +
                    '<div class="form-group">' +
                    '<label>Satuan</label>' +
                    '<input type="text" class="form-control" id="satuan' + no + '" name="satuan[]" required placeholder="Masukan satuan output dari indikator. Contoh: Laporan." />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 1</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '1" name="triwulan1[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 2</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '2" name="triwulan2[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 3</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '3" name="triwulan3[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 4</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '4" name="triwulan4[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-1">' +
                    '<button type="button" class="btn btn-danger h-100" onclick="delFormIndokator(' + no + ')"><i class="fas fa-trash-alt font-16"></i></button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div><hr id="hrFormIndikator' + no + '">'

                const element = document.getElementById("divFormIndikator");
                var container = document.createElement("div");
                container.innerHTML = html;
                element.appendChild(container);
                // console.log(html)

            });

            $(".select2").select2({
                width: '100%'
            });
        });

        function delFormIndokator(no) {
            $("#formIndikator" + no).remove();
            $("#hrFormIndikator" + no).remove();
            // $("#val_varian" + id).remove();
            // console.log('delBadge')
            console.log(no)
        }
    </script>
</body>

</html>