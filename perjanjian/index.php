<?php
include("../inc/pdo.conf.php");

session_start();
$_SESSION["pihak"] = 1;
$_SESSION["id_pegawai"] = 1;

$id = $_SESSION["id_pegawai"];

$currentMenu = 'perjanjian';
$path = '../';

$queryPerjanjian = $db->query("SELECT * FROM `perjanjian_kinerja` WHERE `pihak1`='$id' ORDER BY `tahun` DESC");
$data_perjanjian = $queryPerjanjian->fetchAll(PDO::FETCH_ASSOC);


// echo '<pre>';
// print_r($data_perjanjian);
// echo '</pre>';
// exit();
?>

<!DOCTYPE html>
<html lang="en">

<?php
include($path . "template/header.php");
?>

<body>

    <!-- Top Bar Start -->
    <?php
    include($path . "template/topbar.php");
    ?>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php
        include($path . "template/sidenav.php");
        ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Perjanjian Kinerja</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Perjanjian Kinerja</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" style="width: 50%;">
                                <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                                    <h4 class="mt-2 header-title">Sebagai pihak ke 1</h4>
                                </a>
                            </li>
                            <li class="nav-item" style="width: 50%;">
                                <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                                    <h4 class="mt-2 header-title">Sebagai pihak ke 2</h4>
                                </a>
                            </li>
                        </ul> -->
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="d-flex" style="flex-direction: column;">
                                        <h4 class="mt-0 header-title">Daftar Perjanjian Kinerja</h4>
                                        <p class="text-muted mb-3">List semua perjanjian kinerja yang telah dibuat.</p>
                                    </div>
                                    <a href="input_perjanjian.php" style="height: max-content; margin-right:0px; margin-left:auto"><button class="btn btn-sm btn-primary">Buat Perjanjian</button></a>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div>
                                            <table id="datatable" class="table table-hover table-bordered mb-0 text-center dt-responsive nowrap">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th width="5%">No</th>
                                                        <th width="8%">Tahun</th>
                                                        <th width="30%">Pihak Kedua</th>
                                                        <th width="10%">Indikator</th>
                                                        <th width="15%">Status</th>
                                                        <th width="20%">Monitoring dan Evaluasi</th>
                                                        <th width="15%">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($data_perjanjian as $i => $pk) {

                                                        $idIndikator = $pk['id_perjanjian'];
                                                        $indikator =  $db->query("SELECT id_perjanjian FROM indikator_kinerja WHERE id_perjanjian='$idIndikator'");
                                                        $jumIndikator = $indikator->rowCount();

                                                        if ($pk['status'] == 'disetujui') {
                                                            $status = '<span class="badge badge-soft-success">Disetujui</span>';
                                                        } else if ($pk['status'] == 'revisi') {
                                                            $status = '<span class="badge badge-soft-warning">Revisi</span>';
                                                        } else if ($pk['status'] == 'ditolak') {
                                                            $status = '<span class="badge badge-soft-danger">Ditolak</span>';
                                                        } else if ($pk['status'] == 'belum diajukan') {
                                                            $status = '<span class="badge badge-soft-secondary">Belum diajukan</span>';
                                                        } else if ($pk['status'] == 'diajukan') {
                                                            $status = '<span class="badge badge-soft-dark">Diajukan</span>';
                                                        }


                                                        echo '<tr>
                                                        <td>' . ($i + 1) . '</td>
                                                        <td>' . $pk['tahun'] . '</td>
                                                        <td><b>' . $pk['nama_pihak2'] . '</b><br>(' . $pk['jabatan_pihak2'] . ')</td>
                                                        <td>' . $jumIndikator . ' Indikator</td>
                                                        <td>' . $status . '</td>';

                                                        if ($pk['status'] == 'disetujui') {
                                                            echo '<td> <button class="btn btn-xs btn-success btn-hasil-monev" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" data-id="' . $pk['id_perjanjian'] . '" style="height: max-content; margin-right:0px; margin-left:auto">Lihat hasil monev</button>
                                                        </td>';
                                                        } else {
                                                            echo '<td></td>';
                                                        }

                                                        //     echo '<td><div class="btn-group mb-4 mt-2" role="group" aria-label="Basic example">
                                                        //     <a href="detail_perjanjian.php?p=' . $pk['id_perjanjian'] . '" class="btn btn-xs btn-dark btn-block mr-2 text-white"><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-eyes text-warning font-16"></i></button>
                                                        //     <button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="fas fas fa-print text-primary font-16"></i></button>
                                                        //     <button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Ajukan"><i class="fas fas fa-paper-plane text-success font-16"></i></button>
                                                        // </div></td>';

                                                        if ($pk['status'] == 'belum diajukan' || $pk['status'] == 'revisi') {

                                                            echo '<td><div class="btn-group mb-4 mt-2" role="group" aria-label="Basic example" style="margin-top: auto !important; margin-bottom: auto !important;">
                                                            <a href="detail_perjanjian.php?p=' . $pk['id_perjanjian'] . '"><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Detail"><i class="far fa-list-alt text-dark font-16"></i></button></a>
                                                            <a href="cetak_perjanjian.php?p=' . $pk['id_perjanjian'] . '" target="_blank"><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="fas fas fa-print text-primary font-16"></i></button></a>
                                                            <a href="" class="ajukan-perjanjian" data-toggle="modal" data-animation="bounce" data-target="#modal-ajukan" data-id="' . $pk['id_perjanjian'] . '"><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Ajukan"><i class="fas fas fa-paper-plane text-success font-16"></i></button></a>
                                                            </td></div>';
                                                            // echo '<td><a href="input_perjanjian.php?p=' . $pk['id_perjanjian'] . '" class="btn btn-xs btn-warning btn-block mr-2 text-white">Edit</a>
                                                            // <button type="button" class="btn btn-xs btn-danger btn-block mr-2 text-white btn-hapus-perjanjian" data-id="' . $pk['id_perjanjian'] . '" data-tahun="' . $pk['tahun'] . '">Hapus</button>
                                                            // <a href="cetak_perjanjian.php?p=' . $pk['id_perjanjian'] . '" target="_blank" class="btn btn-xs btn-primary btn-block mr-2 text-white">Cetak</a>
                                                            // <a href="#" class="btn btn-xs btn-success btn-block mr-2 text-white ajukan-perjanjian" data-toggle="modal" data-animation="bounce" data-target="#modal-ajukan" data-id="' . $pk['id_perjanjian'] . '">Ajukan</a></td>';
                                                            // } else if ($pk['status'] == 'revisi') {
                                                            //     echo '<td><a href="input_perjanjian.php?p=' . $pk['id_perjanjian'] . '" class="btn btn-xs btn-warning btn-block mr-2 text-white">Revisi</a>
                                                            //     <a href="cetak_perjanjian.php" target="_blank" class="btn btn-xs btn-primary btn-block mr-2 text-white">Cetak</a>
                                                            //     <a href="#" class="btn btn-xs btn-success btn-block mr-2 text-white" data-toggle="modal" data-animation="bounce" data-target="#modal-ajukan">Ajukan</a></td>';
                                                        } else {

                                                            echo '<td>
                                                            <div class="btn-group mb-4 mt-2" role="group" aria-label="Basic example" style="margin-top: auto !important; margin-bottom: auto !important;">
                                                            <a href="detail_perjanjian.php?p=' . $pk['id_perjanjian'] . '"><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Detail"><i class="far fa-list-alt text-dark font-16"></i></button></a>
                                                            <a href="download_perjanjian.php?p=' . $pk['id_perjanjian'] . '" target="_blank"><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Download"><i class="fas fa-download text-secondary font-16"></i></button></a>
                                                            </td></div>
                                                        </td>';
                                                        }
                                                        echo '</tr>';
                                                    }
                                                    ?>
                                                    <!-- <tr>
                                                        <td>3</td>
                                                        <td>2022</td>
                                                        <td><b>Iwan Setiawan</b><br>(Kepala Sub. Bagian Tata Usaha)</td>
                                                        <td>15 Indikator</td>
                                                        <td>Disetujui</td>
                                                        <td> <button class="btn btn-xs btn-success btn-hasil-monev" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" style="height: max-content; margin-right:0px; margin-left:auto">Lihat hasil monev</button>
                                                        </td>
                                                        <td>
                                                            <a href="detail_perjanjian.php?p=1" class="btn btn-xs btn-dark btn-block mr-2 text-white">Lihat</a>
                                                            <a href="#" class="btn btn-xs btn-secondary btn-block mr-2 text-white">Download</a>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>2022</td>
                                                        <td><b>Iwan Setiawan</b><br>(Kepala Sub. Bagian Tata Usaha)</td>
                                                        <td>15 Indikator</td>
                                                        <td>Diajukan</td>
                                                        <td>-</td>
                                                        <td>
                                                            <a href="#" class="btn btn-xs btn-dark btn-block mr-2 text-white">Lihat</a>
                                                            <a href="#" class="btn btn-xs btn-secondary btn-block mr-2 text-white">Download</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>2022</td>
                                                        <td><b>Iwan Setiawan</b><br>(Kepala Sub. Bagian Tata Usaha)</td>
                                                        <td>15 Indikator</td>
                                                        <td>Revisi</td>
                                                        <td>-</td>
                                                        <td>
                                                            <a href="input_perjanjian.php?p=19" class="btn btn-xs btn-warning btn-block mr-2">Revisi</a>
                                                            <a href="cetak_perjanjian.php" target="_blank" class="btn btn-xs btn-primary btn-block mr-2 text-white">Cetak</a>
                                                            <a href="#" class="btn btn-xs btn-success btn-block mr-2 text-white" data-toggle="modal" data-animation="bounce" data-target="#modal-ajukan">Ajukan</a>
                                                        </td>
                                                    </tr> -->
                                                    <!-- <tr>
                                                        <td>6</td>
                                                        <td>2022</td>
                                                        <td><b>Iwan Setiawan</b><br>(Kepala Sub. Bagian Tata Usaha)</td>
                                                        <td>15 Indikator</td>
                                                        <td>Belum diajukan</td>
                                                        <td>-</td>
                                                        <td>
                                                            <div class="btn-group mb-4 mt-2" role="group" aria-label="Basic example">
                                                                <a href=""><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Detail"><i class="far fa-list-alt text-dark font-16"></i></button></a>
                                                                <a href=""><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Cetak"><i class="fas fas fa-print text-primary font-16"></i></button></a>
                                                                <a href=""><button type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Ajukan"><i class="fas fas fa-paper-plane text-success font-16"></i></button></a>
                                                            </div>
                                                        </td>
                                                    </tr> -->
                                                </tbody>
                                            </table>
                                            <table id="table_perjanjian1" class="table table-hover table-bordered mb-0 text-center dt-responsive nowrap">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th width="5%">No</th>
                                                        <th width="8%">Tahun</th>
                                                        <th width="30%">Pihak Kedua</th>
                                                        <th width="10%">Indikator</th>
                                                        <th width="15%">Status</th>
                                                        <th width="20%">Monitoring dan Evaluasi</th>
                                                        <th width="15%">Aksi</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <!--end table-responsive-->
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>

                <!--  Modal content for the above example -->
                <div class="modal fade bs-example-modal-lg" id="modal-hasil-monev" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0" id="myLargeModalLabel">Daftar Monitoring dan Evaluasi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered mb-0" id="table_hasil_monev">
                                                <thead class="thead-light">
                                                    <tr class="text-center">
                                                        <th width="5%">Triwulan</th>
                                                        <th width="25%">Status</th>
                                                        <th width="25%">Hasil Penilaian</th>
                                                        <th width="25%">Tanggal Monev</th>
                                                        <th width="20%" class="text-center">Dokumen</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="text-center">
                                                        <td>1</td>
                                                        <td><span class="badge badge-soft-success">Selesai</span></td>
                                                        <td><b>98%</b><br>(Berhasil)</td>
                                                        <td>29-03-2022</td>
                                                        <td> <button class="btn btn-xs btn-success" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" style="height: max-content; margin-right:0px; margin-left:auto"><i class="fas fa-download"></i> Download</button>
                                                        </td>
                                                    </tr>
                                                    <tr class="text-center">
                                                        <td>2</td>
                                                        <td><span class="badge badge-soft-primary">Sudah dimonev</span></td>
                                                        <td><b>100%</b><br>(Sangat Berhasil)</td>
                                                        <td>30-06-2022</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr class="text-center">
                                                        <td>3</td>
                                                        <td><span class="badge badge-soft-danger">Belum dimonev</span></td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr class="text-center">
                                                        <td>4</td>
                                                        <td><span class="badge badge-soft-danger">Belum dimonev</span></td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--end table-responsive-->
                                    </div>
                                    <!--end col-->
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="modal fade bs-example-modal-lg" id="modal-ajukan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0" id="myLargeModalLabel">Ajukan Perjanjian Kinerja</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form action="ajukan_perjanjian.php" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="mt-0 header-title">Upload file perjanjian kinerja</h4>
                                                    <p class="text-muted mb-3">File yang diupload harus sudah ditandatangani pihak kesatu.</p>
                                                    <input type="hidden" name="id_ajukan" id="id_ajukan" class="from-group" />
                                                    <input type="file" name="file_pk" id="file_pk" class="dropify" required />
                                                </div>
                                                <!--end card-body-->
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-right">
                                            <button type="button" class="btn btn-sm btn-success" id="btn-ajukan" style="width: 100px;">Ajukan</button>
                                        </div>
                                        <!--end col-->
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div><!-- container -->

            <footer class="footer text-center text-sm-left">
                &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php
    include($path . "template/footer.php");
    ?>

    <script type="text/javascript">
        $(function() {
            $('.ajukan-perjanjian').click(function() {
                var dataId = $(this).attr("data-id");
                $('#id_ajukan').val(dataId)

                console.log('dataId')
                console.log(dataId)

            });

            $('.btn-hapus-perjanjian').click(function() {
                var dataId = $(this).attr("data-id");
                var tahun = $(this).attr("data-tahun");

                console.log('dataId')
                console.log(dataId)

                var fd = new FormData();
                fd.append("id", dataId);

                swal.fire({
                    title: 'Hapus Perjanjian.',
                    text: "Yakin hapus perjanjian kinerja tahun " + tahun + " ?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Hapus!',
                    cancelButtonText: 'Batal',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {

                        $.ajax({
                            type: 'POST',
                            url: 'ajax_data/delete_perjanjian.php',
                            data: fd,
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function(response) {
                                var response = JSON.parse(response);
                                console.log(response);

                                swal.fire(
                                    'Hapus Perjanjian Kinerja!',
                                    'Perjanjian kinerja berhasil dihapus.',
                                    'success'
                                ).then((result) => {
                                    window.location = "index.php";
                                })
                            }
                        });
                    }
                })
            });
        });

        var master_obat = $('#table_perjanjian1').DataTable({
            "processing": true,
            "language": {
                "processing": "Loading. Please Wait...",
            },
            "serverSide": true,
            "ajax": "ajax_data/data_perjanjian_pihak1.php",
            "columns": [{
                    "searchable": true,
                    "data": 'id_perjanjian'
                },
                {
                    "searchable": true,
                    "data": 'tahun'
                },
                {
                    "searchable": true,
                    "data": 'nama_pihak2'
                },
                {
                    "searchable": false,
                    "data": null,
                    "render": function(data, type, full, meta) {
                        var label_text = "";
                        if (data.status_permintaan == 'ruangan') {
                            label_text += '<span style="font-size:14px" class="label label-warning">Belum dikirim ke farmasi</span>';
                        } else if (data.status_permintaan == 'farmasi') {
                            label_text += '<span style="font-size:14px" class="label bg-purple">Menunggu Konfirmasi Farmasi</span>';
                        } else if (data.status_permintaan == 'sedang diproses') {
                            label_text += '<span style="font-size:14px" class="label bg-maroon">Sedang diproses..</span>';
                        } else {
                            //selesai
                            label_text += '<span style="font-size:14px" class="label label-success">Selesai</span>';
                        }
                        return label_text;
                    }
                },
                {
                    "searchable": false,
                    "data": null,
                    "render": function(data, type, full, meta) {
                        var btn = '';
                        var kirim_resep = '';
                        var feedback = "";

                        return 'yuni';
                    }
                },
            ],
            "order": [
                [1, 'asc']
            ],
        });

        function statusMonev(nilai) {
            if (nilai >= 100) {
                status = 'Sangat Berhasil';
            } else if (nilai > 90 && nilai < 100) {
                status = 'Berhasil';
            } else if (nilai >= 60 && nilai <= 90) {
                status = 'Kurang Berhasil';
            } else if (nilai < 60) {
                status = 'Tidak Berhasil';
            }

            return status
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('-');
        }

        $("#btn-ajukan").click(function() {

            var id_ajukan = $("#id_ajukan").val();
            var myFile = $('#file_pk').prop('files')[0];

            // id = $(this).attr("data-id");
            var fd = new FormData();
            fd.append("id_ajukan", id_ajukan);
            fd.append("file_pk", myFile);

            console.log(id_ajukan)
            console.log(myFile)
            $.ajax({
                type: 'POST',
                url: 'ajax_data/ajukan_perjanjian.php',
                data: fd,
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {

                    console.log(response)

                    swal.fire({
                        title: "Perjanjian Kinerja",
                        text: "Perjanjian kinerja berhasil diajukan.",
                        type: 'success',
                    }).then((result) => {
                        window.location = "index.php";
                    })
                }
            });
        });

        $(".btn-hasil-monev").click(function() {
            id = $(this).attr("data-id");
            var fd = new FormData();
            fd.append("id", id);

            var romawi = ['', 'I', 'II', 'III', 'IV']

            console.log(id)
            $.ajax({
                type: 'POST',
                url: 'ajax_data/list_monev.php',
                data: fd,
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    var response = JSON.parse(response);
                    console.log(response);
                    console.log(response['triwulan']);

                    html = '<table class="table table-hover table-bordered mb-0">' +
                        '<thead class="thead-light">' +
                        '<tr class="text-center">' +
                        '<th width="5%">Triwulan</th>' +
                        '<th width="20%">Status</th>' +
                        '<th width="25%">Hasil Penilaian</th>' +
                        '<th width="25%">Tanggal Monev</th>' +
                        '<th width="25%" class="text-center">Aksi</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>'

                    for (i = 1; i < 5; i++) {



                        html += '<tr class="text-center">' +
                            '<td>' + romawi[i] + '</td>'

                        if (response[i]['status'] == '0') {
                            html += '<td><span class="badge badge-soft-primary">Telah dimonev</span></td>' +
                                '<td><b>' + response[i]['persentase'] + '%</b><br>(' + statusMonev(response[i]['persentase']) + ')</td>' +
                                '<td>' + formatDate(response[i]['created_at']) + '</td>' +
                                '<td>' +
                                '<a href="detail_monev.php?m=' + response[i]['id_monev'] + '&t=1" class="btn btn-xs btn-primary btn-block mr-2 text-white">Lihat</a>'
                        } else if (response[i]['status'] == '1') {
                            html += '<td><span class="badge badge-soft-success">Selesai</span></td>' +
                                '<td><b>' + response[i]['persentase'] + '%</b><br>(' + statusMonev(response[i]['persentase']) + ')</td>' +
                                '<td>29-03-2022</td>' +
                                '<td>' +
                                '<a href="detail_monev.php?m=' + response[i]['id_monev'] + '&t=' + i + '" class="btn btn-xs btn-primary btn-block mr-2 text-white">Lihat</a>' +
                                '<a href="download_monev.php?m=' + response[i]['id_monev'] +
                                '" class="btn btn-xs btn-success btn-block mr-2 text-white">Download</a>'
                        } else {
                            html += '<td><span class="badge badge-soft-info">Belum dimonev</span></td>' +
                                '<td>-</td>' +
                                '<td>-</td>' +
                                '<td>-'

                        }

                        html += '</td>' +
                            '</tr>'

                    }

                    '</tbody>' +
                    '</table>'

                    const element = document.getElementById("table_hasil_monev");
                    var container = document.createElement("div");
                    container.innerHTML = html;
                    element.innerHTML = ''
                    element.appendChild(container);
                    // swal.fire(
                    //     'Deleted!',
                    //     'Your file has been deleted.',
                    //     'success'
                    // )
                }
            });
        });
    </script>

</body>

</html>