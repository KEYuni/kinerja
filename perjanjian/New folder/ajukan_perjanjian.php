<?php
include("../inc/pdo.conf.php");
session_start();
// include("../../inc/version.php");
// date_default_timezone_set("Asia/Jakarta");
// $namauser = $_SESSION['namauser'];
// $password = $_SESSION['password'];
// $tipe = $_SESSION['tipe'];
// $id_pegawai = $_SESSION['id_pegawai'];
// $tipes = explode('-', $tipe);
// if ($tipes[0] != 'Dokter_module') {
//     unset($_SESSION['tipe']);
//     unset($_SESSION['namauser']);
//     unset($_SESSION['password']);
//     header("location:../../index.php?status=2");
//     exit;
// }
// include "../../inc/anggota_check.php";
$dataPihak2 = array(
    // 'id_pegawai' => '2',
    // 'nama' => 'Iwang Suwangsih, SE',
    // 'jabatan' => 'PLT. Kepala Sub. Bagian Perencanaan dan Anggaran',
    // 'nik' => '198004282007012018',
    // 'golongan' => 'Penata Muda Tk.I'
    'id_pegawai' => '3',
    'nama' => 'Iwan Setiawan',
    'jabatan' => 'PLT. Kepala Sub. Bagian Tata Usaha',
    'nik' => '196509291988031008',
    'golongan' => 'Penata Tk.I, III/d',
);



$id_perjanjian = isset($_POST['id_ajukan']) ? $_POST['id_ajukan'] : '';

$pengantar_tindakan = $db->query("SELECT nama_pihak1, tahun FROM perjanjian_kinerja WHERE id_perjanjian='" . $id_perjanjian . "'");
$dataPerjanjian = $pengantar_tindakan->fetch(PDO::FETCH_ASSOC);
// echo '<pre>';
// print_r($dataPerjanjian);
// echo '</pre>';
// exit();

$file_tmp = $_FILES['file_pk']['tmp_name'];
$typeFile = explode('/', $_FILES['file_pk']['type']);

$namaUser = str_replace(' ', '_', $dataPerjanjian['nama_pihak1']);
$nama = 'PK_BLUD_' . $namaUser . '_' . $dataPerjanjian['tahun'] . '.' . $typeFile[1];

$pathFile = '../upload/' . $nama;
if (file_exists($pathFile)) {
    // echo 'ada';
    unlink($pathFile);
}

move_uploaded_file($file_tmp, '../upload/' . $nama);
$status = 'diajukan';

$ins = $db->prepare("UPDATE `perjanjian_kinerja` SET `status`=:status1, `dokumen`=:dokumen WHERE `id_perjanjian`=:id_perjanjian");

$ins->bindParam(":id_perjanjian", $id_perjanjian, PDO::PARAM_INT);
$ins->bindParam(":status1", $status, PDO::PARAM_STR);
$ins->bindParam(":dokumen", $nama, PDO::PARAM_STR);

$ins->execute();
// echo '<pre>';
// print_r($id_perjanjian);
// echo '</pre>';
// echo '<pre>';
// print_r($status);
// echo '</pre>';
// echo '<pre>';
// print_r($nama);
// echo '</pre>';
// exit();

header("location:index.php");
