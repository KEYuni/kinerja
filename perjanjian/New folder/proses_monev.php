<?php
include("../inc/pdo.conf.php");
session_start();

$id_perjanjian = isset($_POST['id_perjanjian']) ? $_POST['id_perjanjian'] : '';
$triwulan = isset($_POST['triwulan']) ? $_POST['triwulan'] : '';
$id_indikator = isset($_POST['id_indikator']) ? $_POST['id_indikator'] : '';
$target = isset($_POST['target']) ? $_POST['target'] : '';
$realisasi = isset($_POST['realisasi']) ? $_POST['realisasi'] : '';
$label_realisasi = 'realisasi_' . $triwulan;

$presentase = 0;

foreach ($id_indikator as $i => $id) {

    $realisasiTriwulan = $realisasi[$i];

    $ins = $db->prepare("UPDATE `indikator_kinerja` SET `$label_realisasi`=:$label_realisasi WHERE `id_indikator`=:id_indikator");
    $ins->bindParam(":id_indikator", $id, PDO::PARAM_INT);
    $ins->bindParam(":$label_realisasi", $realisasiTriwulan, PDO::PARAM_INT);

    $ins->execute();

    $persen = ($realisasiTriwulan / $target[$i]) * 100;
    $presentase = $presentase + $persen;
}

$presentase_akhir = $presentase / count($target);

// echo '<pre>';
// print_r($presentase_akhir);
// echo '</pre>';
// exit();

$qMonev = $db->query("SELECT * FROM `monev` WHERE `id_perjanjian`=$id_perjanjian AND `triwulan`=$triwulan");
$cekMonev = $qMonev->rowCount();
$kosong = '';
$status = '0';
// echo '<pre>';
// print_r($id_perjanjian);
// echo '</pre>';
// echo '<pre>';
// print_r($triwulan);
// echo '</pre>';
// echo '<pre>';
// print_r($cekMonev);
// echo '</pre>';

// exit();

if ($cekMonev > 0) {
    $monev = $db->query("SELECT * FROM `monev` WHERE `id_perjanjian`='$id_perjanjian' AND `triwulan`='$triwulan'");
    $dataMonev = $monev->fetch(PDO::FETCH_ASSOC);

    // echo '<pre>';
    // print_r($dataMonev);
    // echo '</pre>';
    // echo '<pre>';
    // print_r($presentase_akhir);
    // echo '</pre>';

    // exit();

    $id_monev = $dataMonev['id_monev'];
    $qMonev = $db->prepare("UPDATE `monev` SET `persentase`=:persentase, `status`=:status1 WHERE `id_monev`='$id_monev'");
    $qMonev->bindParam(":persentase", $presentase_akhir, PDO::PARAM_INT);
    $qMonev->bindParam(":status1", $status, PDO::PARAM_STR);
} else {
    $qMonev = $db->prepare("INSERT INTO `monev` (`id_perjanjian`,`persentase`,`dokumen`,`status`,`triwulan`) VALUES (:id_perjanjian,:persentase, :dokumen, :status1, :triwulan)");
    $qMonev->bindParam(":id_perjanjian", $id_perjanjian, PDO::PARAM_INT);
    $qMonev->bindParam(":persentase", $presentase_akhir, PDO::PARAM_INT);
    $qMonev->bindParam(":dokumen", $kosong, PDO::PARAM_STR);
    $qMonev->bindParam(":status1", $status, PDO::PARAM_STR);
    $qMonev->bindParam(":triwulan", $triwulan, PDO::PARAM_INT);
}

$qMonev->execute();

header("location:index2.php");
