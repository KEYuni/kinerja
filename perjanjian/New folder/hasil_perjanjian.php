<?php
include("../inc/pdo.conf.php");
session_start();
// include("../../inc/version.php");
// date_default_timezone_set("Asia/Jakarta");
// $namauser = $_SESSION['namauser'];
// $password = $_SESSION['password'];
// $tipe = $_SESSION['tipe'];
// $id_pegawai = $_SESSION['id_pegawai'];
// $tipes = explode('-', $tipe);
// if ($tipes[0] != 'Dokter_module') {
//     unset($_SESSION['tipe']);
//     unset($_SESSION['namauser']);
//     unset($_SESSION['password']);
//     header("location:../../index.php?status=2");
//     exit;
// }
// include "../../inc/anggota_check.php";
$dataPihak2 = array(
    // 'id_pegawai' => '2',
    // 'nama' => 'Iwang Suwangsih, SE',
    // 'jabatan' => 'PLT. Kepala Sub. Bagian Perencanaan dan Anggaran',
    // 'nik' => '198004282007012018',
    // 'golongan' => 'Penata Muda Tk.I'
    'id_pegawai' => '3',
    'nama' => 'Iwan Setiawan',
    'jabatan' => 'PLT. Kepala Sub. Bagian Tata Usaha',
    'nik' => '196509291988031008',
    'golongan' => 'Penata Tk.I, III/d',
);



$id_perjanjian = isset($_POST['id_perjanjian']) ? $_POST['id_perjanjian'] : '';
$hasil_perjanjian = isset($_POST['hasil_perjanjian']) ? $_POST['hasil_perjanjian'] : '';

$pengantar_tindakan = $db->query("SELECT nama_pihak1, tahun FROM perjanjian_kinerja WHERE id_perjanjian='" . $id_perjanjian . "'");
$dataPerjanjian = $pengantar_tindakan->fetch(PDO::FETCH_ASSOC);

$ket_revisi = '';
$alasan_tolak = '';
$nama = '';

if ($hasil_perjanjian == 'setuju') {
    $status = 'disetujui';
    print_r('setuju yaaaa');

    $file_tmp = $_FILES['file_setuju']['tmp_name'];
    $typeFile = explode('/', $_FILES['file_setuju']['type']);

    $namaUser = explode(' ', $dataPerjanjian['nama_pihak1']);
    $nama = 'PK_BLUD_' . $namaUser[0] . '_' . $namaUser[1] . '_' . $dataPerjanjian['tahun'] . '.' . $typeFile[1];

    $pathFile = '../upload/' . $nama;
    if (file_exists($pathFile)) {
        // echo 'ada';
        unlink($pathFile);
    }

    move_uploaded_file($file_tmp, '../upload/' . $nama);
} else if ($hasil_perjanjian == 'revisi') {
    $status = 'revisi';
    $ket_revisi = isset($_POST['ket_revisi']) ? $_POST['ket_revisi'] : '';

    print_r('perbaiki dulu');
} else {
    $status = 'ditolak';
    $alasan_tolak = isset($_POST['alasan_tolak']) ? $_POST['alasan_tolak'] : '';
    print_r('maaf');
}
// echo '<pre>';
// print_r($_POST);
// echo '</pre>';
// exit();

if ($hasil_perjanjian == 'setuju') {
    $ins = $db->prepare("UPDATE `perjanjian_kinerja` SET `status`=:status1, `alasan_tolak`=:alasan_tolak, `ket_revisi`=:ket_revisi, `dokumen`=:dokumen WHERE `id_perjanjian`=:id_perjanjian");
    $ins->bindParam(":dokumen", $nama, PDO::PARAM_STR);
} else {
    $ins = $db->prepare("UPDATE `perjanjian_kinerja` SET `status`=:status1, `alasan_tolak`=:alasan_tolak, `ket_revisi`=:ket_revisi WHERE `id_perjanjian`=:id_perjanjian");
}

$ins->bindParam(":id_perjanjian", $id_perjanjian, PDO::PARAM_INT);
$ins->bindParam(":status1", $status, PDO::PARAM_STR);
$ins->bindParam(":ket_revisi", $ket_revisi, PDO::PARAM_STR);
$ins->bindParam(":alasan_tolak", $alasan_tolak, PDO::PARAM_STR);

$ins->execute();

header("location:index2.php");
