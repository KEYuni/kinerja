<?php
include("../inc/pdo.conf.php");
session_start();
// include("../../inc/version.php");
// date_default_timezone_set("Asia/Jakarta");
// $namauser = $_SESSION['namauser'];
// $password = $_SESSION['password'];
// $tipe = $_SESSION['tipe'];
// $id_pegawai = $_SESSION['id_pegawai'];
// $tipes = explode('-', $tipe);
// if ($tipes[0] != 'Dokter_module') {
//     unset($_SESSION['tipe']);
//     unset($_SESSION['namauser']);
//     unset($_SESSION['password']);
//     header("location:../../index.php?status=2");
//     exit;
// }
// include "../../inc/anggota_check.php";
$dataPihak2 = array(
    // 'id_pegawai' => '2',
    // 'nama' => 'Iwang Suwangsih, SE',
    // 'jabatan' => 'PLT. Kepala Sub. Bagian Perencanaan dan Anggaran',
    // 'nik' => '198004282007012018',
    // 'golongan' => 'Penata Muda Tk.I'
    'id_pegawai' => '3',
    'nama' => 'Iwan Setiawan',
    'jabatan' => 'PLT. Kepala Sub. Bagian Tata Usaha',
    'nik' => '196509291988031008',
    'golongan' => 'Penata Tk.I, III/d',
);


//Data perjanjian kinerja
$pihak1 = isset($_SESSION['id_pegawai']) ? $_SESSION['id_pegawai'] : '';
$nama_pihak1 = isset($_SESSION['nama']) ? $_SESSION['nama'] : '';
$jabatan_pihak1 = isset($_SESSION['jabatan']) ? $_SESSION['jabatan'] : '';
$nik_pihak1 = isset($_SESSION['nik']) ? $_SESSION['nik'] : '';
$golongan_pihak1 = isset($_SESSION['golongan']) ? $_SESSION['golongan'] : '';
$idPihak2 = isset($_POST['pihak2']) ? $_POST['pihak2'] : '';
$pihak2 = isset($dataPihak2['id_pegawai']) ? $dataPihak2['id_pegawai'] : '';
$nama_pihak2 = isset($dataPihak2['nama']) ? $dataPihak2['nama'] : '';
$jabatan_pihak2 = isset($dataPihak2['jabatan']) ? $dataPihak2['jabatan'] : '';
$nik_pihak2 = isset($dataPihak2['nik']) ? $dataPihak2['nik'] : '';
$golongan_pihak2 = isset($dataPihak2['golongan']) ? $dataPihak2['golongan'] : '';
$tahun = isset($_POST['tahun']) ? $_POST['tahun'] : '';
$status = isset($_POST['status']) ? $_POST['status'] : '';
$alasan_tolak = isset($_POST['alasan_tolak']) ? $_POST['alasan_tolak'] : '';
$ket_revisi = isset($_POST['ket_revisi']) ? $_POST['ket_revisi'] : '';
$dokumen = isset($_POST['dokumen']) ? $_POST['dokumen'] : '';

//indikator perjanjian kinerja
$id_perjanjian = '';
$indikator = isset($_POST['indikator']) ? $_POST['indikator'] : '';
$satuan = isset($_POST['satuan']) ? $_POST['satuan'] : 'belum diajukan';
$triwulan1 = isset($_POST['triwulan1']) ? $_POST['triwulan1'] : '';
$triwulan2 = isset($_POST['triwulan2']) ? $_POST['triwulan2'] : '';
$triwulan3 = isset($_POST['triwulan3']) ? $_POST['triwulan3'] : '';
$triwulan4 = isset($_POST['triwulan4']) ? $_POST['triwulan4'] : '';
$realisasi_1 = isset($dataPihak2['realisasi_1']) ? $dataPihak2['realisasi_1'] : '';
$realisasi_2 = isset($dataPihak2['realisasi_2']) ? $dataPihak2['realisasi_2'] : '';
$realisasi_3 = isset($dataPihak2['realisasi_3']) ? $dataPihak2['realisasi_3'] : '';
$realisasi_4 = isset($dataPihak2['realisasi_4']) ? $dataPihak2['realisasi_4'] : '';


$qpk = $db->query("SELECT * FROM perjanjian_kinerja WHERE pihak1='" . $pihak1 . "' AND tahun='" . $tahun . "' AND status!='ditolak'");
$cekDataPerjanjian = $qpk->rowCount();

// jika surat pengantar tindakan/operasi sudah ada
if ($cekDataPerjanjian > 0) {
    $dataTindakan = $qpk->fetch(PDO::FETCH_ASSOC);
    $id_perjanjian = $dataTindakan['id_perjanjian'];

    $ins = $db->prepare("UPDATE `perjanjian_kinerja` SET `pihak1`=:pihak1, `nama_pihak1` = :nama_pihak1, `jabatan_pihak1`=:jabatan_pihak1, `nik_pihak1`=:nik_pihak1, `golongan_pihak1`=:golongan_pihak1, `pihak2`=:pihak2, `nama_pihak2`=:nama_pihak2, `jabatan_pihak2`=:jabatan_pihak2, `nik_pihak2`=:nik_pihak2, `golongan_pihak2`=:golongan_pihak2, `tahun`=:tahun, `status`=:status1, `alasan_tolak`=:alasan_tolak, `ket_revisi`=:ket_revisi, `dokumen`=:dokumen WHERE `id_perjanjian`=:id_perjanjian ");

    $ins->bindParam(":id_perjanjian", $id_perjanjian, PDO::PARAM_INT);
} else {
    $ins = $db->prepare("INSERT INTO `perjanjian_kinerja` (`pihak1`, `nama_pihak1`, `jabatan_pihak1`, `nik_pihak1`,  `golongan_pihak1`,  `pihak2`,  `nama_pihak2`,  `jabatan_pihak2`,  `nik_pihak2`,  `golongan_pihak2`,  `tahun`,  `status`,  `alasan_tolak`,  `ket_revisi`,  `dokumen`) VALUES (:pihak1, :nama_pihak1, :jabatan_pihak1, :nik_pihak1, :golongan_pihak1, :pihak2, :nama_pihak2, :jabatan_pihak2, :nik_pihak2, :golongan_pihak2, :tahun, :status1, :alasan_tolak, :ket_revisi, :dokumen)");

    $status = 'belum diajukan';
}

$ins->bindParam(":pihak1", $pihak1, PDO::PARAM_INT);
$ins->bindParam(":nama_pihak1", $nama_pihak1, PDO::PARAM_STR);
$ins->bindParam(":jabatan_pihak1", $jabatan_pihak1, PDO::PARAM_STR);
$ins->bindParam(":nik_pihak1", $nik_pihak1, PDO::PARAM_STR);
$ins->bindParam(":golongan_pihak1", $golongan_pihak1, PDO::PARAM_STR);
$ins->bindParam(":pihak2", $pihak2, PDO::PARAM_INT);
$ins->bindParam(":nama_pihak2", $nama_pihak2, PDO::PARAM_STR);
$ins->bindParam(":jabatan_pihak2", $jabatan_pihak2, PDO::PARAM_STR);
$ins->bindParam(":nik_pihak2", $nik_pihak2, PDO::PARAM_STR);
$ins->bindParam(":golongan_pihak2", $golongan_pihak2, PDO::PARAM_STR);
$ins->bindParam(":tahun", $tahun, PDO::PARAM_STR);
$ins->bindParam(":status1", $status, PDO::PARAM_STR);
$ins->bindParam(":alasan_tolak", $alasan_tolak, PDO::PARAM_STR);
$ins->bindParam(":ket_revisi", $ket_revisi, PDO::PARAM_STR);
$ins->bindParam(":dokumen", $dokumen, PDO::PARAM_STR);

$ins->execute();

if ($cekDataPerjanjian <= 0) {
    $last_perjanjian = $db->query("SELECT * FROM perjanjian_kinerja WHERE pihak1='" . $pihak1 . "' AND tahun='" . $tahun . "' ORDER BY id_perjanjian DESC");
    $dataLastPerjanjian = $last_perjanjian->fetch(PDO::FETCH_ASSOC);
    $id_perjanjian = $dataLastPerjanjian['id_perjanjian'];
} else {
    $qIndikator = $db->query("SELECT * FROM `indikator_kinerja` WHERE `id_perjanjian`='$id_perjanjian' ORDER BY `id_indikator` ASC");
    $dataIndikator = $qIndikator->fetchAll(PDO::FETCH_ASSOC);

    for ($i = 0; $i < count($dataIndikator); $i++) {
        $idIndikator = $dataIndikator[$i]['id_perjanjian'];
        $qDelIndikator = $db->query("DELETE FROM indikator_kinerja WHERE `id_perjanjian`='$idIndikator'");
        $qDelIndikator->execute();
    }
}

$queryIndikator = $db->prepare("INSERT INTO `indikator_kinerja` (`id_perjanjian`, `indikator`, `satuan`, `target_1`,  `target_2`,  `target_3`,  `target_4`,  `realisasi_1`,  `realisasi_2`,  `realisasi_3`,  `realisasi_4`) VALUES (:id_perjanjian, :indikator, :satuan, :target_1, :target_2, :target_3, :target_4, :realisasi_1, :realisasi_2, :realisasi_3, :realisasi_4)");

for ($i = 0; $i < count($indikator); $i++) {

    $queryIndikator->bindParam(":id_perjanjian", $id_perjanjian, PDO::PARAM_STR);
    $queryIndikator->bindParam(":indikator", $indikator[$i], PDO::PARAM_STR);
    $queryIndikator->bindParam(":satuan", $satuan[$i], PDO::PARAM_STR);
    $queryIndikator->bindParam(":target_1", $triwulan1[$i], PDO::PARAM_INT);
    $queryIndikator->bindParam(":target_2", $triwulan2[$i], PDO::PARAM_INT);
    $queryIndikator->bindParam(":target_3", $triwulan3[$i], PDO::PARAM_INT);
    $queryIndikator->bindParam(":target_4", $triwulan4[$i], PDO::PARAM_INT);
    $queryIndikator->bindParam(":realisasi_1", $realisasi_1, PDO::PARAM_INT);
    $queryIndikator->bindParam(":realisasi_2", $realisasi_2, PDO::PARAM_INT);
    $queryIndikator->bindParam(":realisasi_3", $realisasi_3, PDO::PARAM_INT);
    $queryIndikator->bindParam(":realisasi_4", $realisasi_4, PDO::PARAM_INT);

    $queryIndikator->execute();
}

if ($_SESSION['pihak'] == 2) {
    header("location:index2.php");
} else {
    header("location:index.php");
}
