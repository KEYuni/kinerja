<?php
include("../inc/pdo.conf.php");
session_start();


$id_monev = isset($_POST['id_monev']) ? $_POST['id_monev'] : '';

$qMonev = $db->query("SELECT * FROM monev as m LEFT JOIN perjanjian_kinerja as pk ON m.id_perjanjian = pk.id_perjanjian WHERE id_monev='" . $id_monev . "'");
$dataMonev = $qMonev->fetch(PDO::FETCH_ASSOC);

$file_tmp = $_FILES['doc_monev']['tmp_name'];
$typeFile = explode('/', $_FILES['doc_monev']['type']);

$namaUser = str_replace(' ', '_', $dataMonev['nama_pihak1']);
$nama = '[MONEV] PK_BLUD_' . $namaUser . '_' . $dataMonev['tahun'] . '.' . $typeFile[1];


// echo '<pre>';
// print_r($dataMonev);
// echo '</pre>';
// echo '<pre>';
// print_r($nama);
// echo '</pre>';
// exit();

$pathFile = '../upload/' . $nama;
if (file_exists($pathFile)) {
    unlink($pathFile);
}

move_uploaded_file($file_tmp, '../upload/' . $nama);
$status = '1';

$ins = $db->prepare("UPDATE `monev` SET `status`=:status1, `dokumen`=:dokumen WHERE `id_monev`=:id_monev");

$ins->bindParam(":id_monev", $id_monev, PDO::PARAM_INT);
$ins->bindParam(":status1", $status, PDO::PARAM_STR);
$ins->bindParam(":dokumen", $nama, PDO::PARAM_STR);

$ins->execute();

header("location:index.php");
