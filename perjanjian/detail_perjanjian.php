<?php
include("../inc/pdo.conf.php");

session_start();

$id = 1;
$pihak = $_SESSION['pihak'];

if ($pihak == 1) {
    $currentMenu = 'perjanjian';
    $mainPage = 'index.php';
} else {
    $currentMenu = 'approval';
    $mainPage = 'index2.php';
}

$path = '../';

$id_perjanjian = isset($_GET["p"]) ? $_GET['p'] : '';

$queryPerjanjian = $db->query("SELECT * FROM `perjanjian_kinerja` WHERE `id_perjanjian`='$id_perjanjian'");
$data_perjanjian = $queryPerjanjian->fetch(PDO::FETCH_ASSOC);

$queryIndikator = $db->query("SELECT * FROM `indikator_kinerja` WHERE `id_perjanjian`='$id_perjanjian' ORDER BY `id_indikator` ASC");
$dataIndikator = $queryIndikator->fetchAll(PDO::FETCH_ASSOC);
// echo '<pre>';
// print_r($id_perjanjian);
// echo '</pre>';
// echo '<pre>';
// print_r($dataIndikator);
// echo '</pre>';
// exit();
?>

<!DOCTYPE html>
<html lang="en">

<?php
include($path . "template/header.php");
?>

<body>

    <!-- Top Bar Start -->
    <?php
    include($path . "template/topbar.php");
    ?>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php
        include($path . "template/sidenav.php");
        ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../dashboard.php">Dashboard</a></li>
                                    <?php if ($pihak == 1) { ?>
                                        <li class="breadcrumb-item"><a href="index.php">Perjanjian Kinerja</a></li>
                                    <?php } else { ?>
                                        <li class="breadcrumb-item"><a href="index2.php">Perjanjian Kinerja</a></li>
                                    <?php } ?>
                                    <li class="breadcrumb-item active">Detail</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Detail Perjanjian Kinerja Tahun <?php echo $data_perjanjian['tahun'] ?></h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->
                <?php if ($data_perjanjian) {
                    if ($data_perjanjian['status'] == 'revisi') { ?>
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="card" style="border: solid 1px #dedede !important">
                                    <div class="card-header bg-warning">
                                        <h4 style="letter-spacing: 1.5px;"><b>KETERANGAN REVISI</b></h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <h5><?php echo $data_perjanjian['ket_revisi'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } else if ($data_perjanjian['status'] == 'ditolak') { ?>
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="card" style="border: solid 1px #dedede !important">
                                    <div class="card-header bg-danger text-white">
                                        <h4>ALASAN DITOLAK</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <h5><?php echo $data_perjanjian['alasan_tolak'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php if ($data_perjanjian['status'] == 'belum diajukan') {
                        $classText = 'text-dark';
                    } else if ($data_perjanjian['status'] == 'diajukan') {
                        $classText = 'text-primary';
                    } else if ($data_perjanjian['status'] == 'revisi') {
                        $classText = 'text-warning';
                    } else if ($data_perjanjian['status'] == 'ditolak') {
                        $classText = 'text-danger';
                    } else if ($data_perjanjian['status'] == 'disetujui') {
                        $classText = 'text-success';
                    }
                } ?>
                <div class="row">
                    <!-- <div class="col-lg-6">
                        <?php if ($data_perjanjian['status'] == 'belum diajukan') { ?>
                            <a><button class="btn btn-danger btn-sm btn-hapus-perjanjian mb-3" style="height: 35px; width:80px" data-id="<?php echo $data_perjanjian['id_perjanjian'] ?>" data-tahun="<?php echo $data_perjanjian['tahun'] ?>"><i class="fas fa-trash"></i> Delete</button>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="col-lg-6 text-right">
                        <?php if ($data_perjanjian['status'] == 'belum diajukan' || $data_perjanjian['status'] == 'revisi') { ?>
                            <a href="input_perjanjian.php?p=<?php echo $data_perjanjian['id_perjanjian'] ?>"><button class="btn btn-warning btn-sm" style="height: 35px; width:80px"><i class="fas fa-edit"></i> Edit</button>
                            </a>
                        <?php } ?>
                        <a href="cetak_perjanjian.php?p=<?php echo $data_perjanjian['id_perjanjian'] ?>" target="_blank">
                            <button class="btn btn-primary btn-sm" style="height: 35px; width:80px"><i class="fas fa-print"></i> Cetak</button>
                        </a>
                    </div> -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <div class="d-flex"> -->
                                <div class="d-flex" style="flex-direction: column;">
                                    <h4 class="mt-0 header-title">Perjanjian Kinerja Tahun <?php echo $data_perjanjian['tahun'] ?></h4>
                                    <p class="text-muted">(dibuat pada 2 Februari 2022 - <span class="<?php echo $classText ?>" style="font-weight: bold;"><?php echo $data_perjanjian['status'] ?></span>)</p>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-lg-4">
                                        <?php if ($data_perjanjian['status'] == 'belum diajukan') { ?>
                                            <a><button class="btn btn-danger btn-sm btn-hapus-perjanjian" style="height: 35px; width:100px" data-id="<?php echo $data_perjanjian['id_perjanjian'] ?>" data-tahun="<?php echo $data_perjanjian['tahun'] ?>"><i class="fas fa-trash"></i> Delete</button>
                                            </a>
                                        <?php } ?>
                                    </div>
                                    <div class="col-lg-8 text-right">
                                        <?php if ($data_perjanjian['status'] == 'belum diajukan' || $data_perjanjian['status'] == 'revisi') { ?>
                                            <a href="input_perjanjian.php?p=<?php echo $data_perjanjian['id_perjanjian'] ?>"><button class="btn btn-warning btn-sm" style="height: 35px; width:100px"><i class="fas fa-edit"></i> Edit</button>
                                            </a>
                                        <?php } ?>
                                        <a href="cetak_perjanjian.php?p=<?php echo $data_perjanjian['id_perjanjian'] ?>" target="_blank">
                                            <button class="btn btn-primary btn-sm" style="height: 35px; width:100px"><i class="fas fa-print"></i> Cetak</button>
                                        </a>
                                    </div>
                                </div>
                                <!-- <div style="margin-right: 0px; margin-left:auto">
                                        <?php if ($data_perjanjian['status'] == 'belum diajukan' || $data_perjanjian['status'] == 'revisi') { ?>
                                            <a href="input_perjanjian.php?p=<?php echo $data_perjanjian['id_perjanjian'] ?>"><button class="btn btn-warning btn-sm" style="height: 35px; width:80px"><i class="fas fa-edit"></i> Edit</button>
                                            </a>
                                        <?php } ?>
                                        <?php if ($data_perjanjian['status'] == 'belum diajukan') { ?>
                                            <a><button class="btn btn-danger btn-sm btn-hapus-perjanjian" style="height: 35px; width:80px" data-id="<?php echo $data_perjanjian['id_perjanjian'] ?>" data-tahun="<?php echo $data_perjanjian['tahun'] ?>"><i class="fas fa-trash"></i> Delete</button>
                                            </a>
                                        <?php } ?>
                                        <a href="cetak_perjanjian.php?p=<?php echo $data_perjanjian['id_perjanjian'] ?>" target="_blank">
                                            <button class="btn btn-primary btn-sm" style="height: 35px; width:80px"><i class="fas fa-print"></i> Cetak</button>
                                        </a>
                                    </div> -->
                                <!-- </div> -->
                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <div class="card" style="border: solid 1px #dedede !important">
                                            <div class="card-header bg-primary">
                                                <h5 class="text-white">PIHAK PERTAMA</h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="d-flex" style="flex-direction: column; margin-right:15px">
                                                        <h6>Nama</h6>
                                                        <h6>Jabatan</h6>
                                                        <h6>NIK</h6>
                                                    </div>
                                                    <div class="d-flex" style="flex-direction: column;">
                                                        <h6>: <?php echo $data_perjanjian['nama_pihak1'] ?></h6>
                                                        <h6>: <?php echo $data_perjanjian['jabatan_pihak1'] ?></h6>
                                                        <h6>: <?php echo $data_perjanjian['nik_pihak1'] ?></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card" style="border: solid 1px #dedede !important">
                                            <div class="card-header bg-success">
                                                <h5 class="text-white">PIHAK KEDUA</h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="d-flex" style="flex-direction: column; margin-right:15px">
                                                        <h6>Nama</h6>
                                                        <h6>Jabatan</h6>
                                                        <h6>NIK</h6>
                                                    </div>
                                                    <div class="d-flex" style="flex-direction: column;">
                                                        <h6>: <?php echo $data_perjanjian['nama_pihak2'] ?></h6>
                                                        <h6>: <?php echo $data_perjanjian['jabatan_pihak2'] ?></h6>
                                                        <h6>: <?php echo $data_perjanjian['nik_pihak2'] ?></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form class="form-parsley" action="#" id="form-perjanjian">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered mb-0">
                                                    <thead class="thead-light">
                                                        <tr class="text-center">
                                                            <th rowspan="2" width="5%">No</th>
                                                            <th rowspan="2" width="15%">Sasaran Strategis</th>
                                                            <th rowspan="2" width="20%">Indikator Kinerja</th>
                                                            <th rowspan="2" width="10%">Satuan</th>
                                                            <th rowspan="2" width="10%">Target<br>Tahun <?php echo $data_perjanjian['tahun'] ?></th>
                                                            <th colspan="4" width="40%">Target Triwulan</th>

                                                            <!-- <th width="10%" class="text-center">Aksi</th> -->
                                                        </tr>
                                                        <tr>
                                                            <th width="10%">I</th>
                                                            <th width="10%">II</th>
                                                            <th width="10%">III</th>
                                                            <th width="10%">IV</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($dataIndikator as $i => $indikator) {
                                                            $targetTahunan = (int)$indikator['target_1'] + (int)$indikator['target_2'] + (int)$indikator['target_3'] + (int)$indikator['target_4']; ?>

                                                            <tr class="text-center">
                                                                <td><?php echo ($i + 1) ?></td>
                                                                <?php if ($i == 0) { ?>
                                                                    <td rowspan="<?php echo count($dataIndikator) ?>">Meningkatnya Kualitas Lingkungan Sehat, Budaya Sehat, dan Mutu Pelayanan Kesehatan</span></td>
                                                                <?php } ?>
                                                                <td><?php echo $indikator['indikator'] ?></td>
                                                                <td><?php echo ucfirst($indikator['satuan']) ?></td>
                                                                <td><?php echo $targetTahunan; ?></td>
                                                                <td><?php echo $indikator['target_1']; ?></td>
                                                                <td><?php echo $indikator['target_2']; ?></td>
                                                                <td><?php echo $indikator['target_3']; ?></td>
                                                                <td><?php echo $indikator['target_4']; ?></td>
                                                                <!-- <td>
                                                                <button class="btn btn-xs btn-success btn-block" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" style="height: max-content; margin-right:0px; margin-left:auto">Input realisasi</button>
                                                            </td> -->
                                                            </tr>

                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-12 text-right">
                                            <button class="btn btn-success mt-3 btn-sm"><i class="fas fa-save"></i> Simpan hasil monev</button>
                                        </div> -->
                                    </div>
                                </form>
                                <div class="mt-4">
                                    <a href="<?php echo $mainPage; ?>">
                                        <button class="btn btn-danger btn-sm" style="height: 35px; width:80px">Kembali</button>
                                    </a>
                                </div>
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->

                </div> <!-- end row -->

            </div><!-- container -->

            <footer class="footer text-center text-sm-left">
                &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery -->
    <?php
    include($path . "template/footer.php");
    ?>

    <script type="text/javascript">
        $(function() {
            //A title with a text under
            $('#btnAddPerjanjian').click(function() {

                tahun = $("#tahun").val();
                pihak2 = $("#pihak2").val();

                var indikator = [];
                $("textarea[name='indikator[]']").each(function() {
                    var text = $(this).val();
                    console.log($(this))
                    indikator.push(text);
                });

                var satuan = $("input[name='satuan[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan1 = $("input[name='triwulan1[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan2 = $("input[name='triwulan2[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan3 = $("input[name='triwulan3[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();

                var triwulan4 = $("input[name='triwulan4[]']")
                    .map(function() {
                        return $(this).val();
                    }).get();
                // var names = [];
                // var textarea = document.querySelector('textarea#indikator');

                // function saveNames() {
                //     names = textarea.value.split('\n');
                // }

                cekIndikator = indikator.includes('');
                cekSatuan = satuan.includes('');
                cekTriwulan1 = triwulan1.includes('');
                cekTriwulan2 = triwulan2.includes('');
                cekTriwulan3 = triwulan3.includes('');
                cekTriwulan4 = triwulan4.includes('');

                console.log(indikator)
                console.log('cekIndikator')
                console.log(cekIndikator)
                // console.log(tahun)
                // console.log(pihak2)
                // console.log(indikator)
                // console.log(satuan)
                // console.log(triwulan1)
                // console.log(triwulan2)
                // console.log(triwulan3)
                // console.log(triwulan4)
                // Swal.fire(
                //     'Berhasil!',
                //     'Perjanjian kinerja berhasil dibuat.',
                //     'success'
                // )
            });
            $('#addFormIndikator').click(function() {
                no = $("#noFormIndikator").val();
                no = parseInt(no) + 1
                $("#noFormIndikator").val(no);
                console.log(no)

                html = '<div class="row px-2" id="formIndikator' + no + '" style="margin-right: 5px;">' +
                    '<div class="col-md-5">' +
                    '<div class="form-group">' +
                    '<label>Indikator Kinerja</label>' +
                    '<textarea required class="form-control" rows="6" id="indikator' + no + '" name="indikator[]"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-7">' +
                    '<div class="row">' +
                    '<div class="col-md-11">' +
                    '<div class="row">' +
                    '<div class="col-md-12 mb-1">' +
                    '<div class="form-group">' +
                    '<label>Satuan</label>' +
                    '<input type="text" class="form-control" id="satuan' + no + '" name="satuan[]" required placeholder="Masukan satuan output dari indikator. Contoh: Laporan." />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 1</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '1" name="triwulan1[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 2</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '2" name="triwulan2[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 3</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '3" name="triwulan3[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="form-group">' +
                    '<label>Target triwulan 4</label>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '4" name="triwulan4[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-1">' +
                    '<button type="button" class="btn btn-danger h-100" onclick="delFormIndokator(' + no + ')"><i class="fas fa-trash-alt font-16"></i></button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div><hr id="hrFormIndikator' + no + '">'

                const element = document.getElementById("divFormIndikator");
                var container = document.createElement("div");
                container.innerHTML = html;
                element.appendChild(container);
                // console.log(html)

            });

            $('.btn-hapus-perjanjian').click(function() {
                var dataId = $(this).attr("data-id");
                var tahun = $(this).attr("data-tahun");

                console.log('dataId')
                console.log(dataId)

                var fd = new FormData();
                fd.append("id", dataId);

                swal.fire({
                    title: 'Hapus Perjanjian.',
                    text: "Yakin hapus perjanjian kinerja tahun " + tahun + " ?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Hapus!',
                    cancelButtonText: 'Batal',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {

                        $.ajax({
                            type: 'POST',
                            url: 'ajax_data/delete_perjanjian.php',
                            data: fd,
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function(response) {
                                var response = JSON.parse(response);
                                console.log(response);

                                swal.fire(
                                    'Hapus Perjanjian Kinerja!',
                                    'Perjanjian kinerja berhasil dihapus.',
                                    'success'
                                ).then((result) => {
                                    window.location = "index.php";
                                })
                            }
                        });
                    }
                })
            });

            $(".select2").select2({
                width: '100%'
            });
        });

        function delFormIndokator(no) {
            $("#formIndikator" + no).remove();
            $("#hrFormIndikator" + no).remove();
            // $("#val_varian" + id).remove();
            // console.log('delBadge')
            console.log(no)
        }
    </script>
</body>

</html>