<?php
include("../inc/pdo.conf.php");

$id_perjanjian = isset($_GET["p"]) ? $_GET['p'] : '';

$pengantar_tindakan = $db->query("SELECT dokumen FROM perjanjian_kinerja WHERE id_perjanjian='" . $id_perjanjian . "'");
$dataPerjanjian = $pengantar_tindakan->fetch(PDO::FETCH_ASSOC);

$dir = "../upload/";
$filename = $dataPerjanjian['dokumen'];
$file_path = $dir . $filename;
$ctype = "application/octet-stream";

if (!empty($file_path) && file_exists($file_path)) { //check keberadaan file
    header("Pragma:public");
    header("Expired:0");
    header("Cache-Control:must-revalidate");
    header("Content-Control:public");
    header("Content-Description: File Transfer");
    header("Content-Type: $ctype");
    header("Content-Disposition:attachment; filename=\"" . basename($file_path) . "\"");
    header("Content-Transfer-Encoding:binary");
    header("Content-Length:" . filesize($file_path));
    flush();
    readfile($file_path);
    header("location:/perjanjian/index.php");

    // exit();
} else {
    header("location:/perjanjian/index.php");

    // echo "The File does not exist.";
}
