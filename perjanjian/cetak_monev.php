<?php
require_once('../plugins/tcpdf/6_2_25/tcpdf.php');
require_once('../plugins/tcpdf/6_2_25/config/tcpdf_config.php');
include("../inc/pdo.conf.php");

function bulan($bulan)
{
    $listBulan = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

    $no = (int) $bulan;
    return $listBulan[$no];
}

class MYPDF extends TCPDF
{
    //Page header
    public function Header()
    {

        $header = '<table>
        <tr>
        <td width="85%"></td>
        <td width="15%" style="border: 0.6px solid black; text-align:center">Form Monev<br>Staf</td></tr>
        </table>';
        $this->writeHTML($header, true, 0, true, 0);
    }

    // Page footer
    public function Footer()
    {
    }
}

// create new PDF document
$pdf = new MYPDF('L', PDF_UNIT, array(215, 330), true, 'UTF-8', false);

//Get id perjanjian kinerja

$romawi = ['', 'I', 'II', 'III', 'IV'];

$id_monev = isset($_GET['m']) ? $_GET['m'] : '';
$triwulan = isset($_GET['t']) ? $_GET['t'] : '';
$indexTarget = 'target_' . $triwulan;
$indexRealisasi = 'realisasi_' . $triwulan;

$qMonev = $db->query("SELECT m.*, m.created_at as waktu_monev, pk.*  FROM monev as m LEFT JOIN perjanjian_kinerja as pk ON m.id_perjanjian = pk.id_perjanjian WHERE id_monev='" . $id_monev . "'");
$data_monev = $qMonev->fetch(PDO::FETCH_ASSOC);
$id_perjanjian = $data_monev['id_perjanjian'];

//Get add indikator dari perjanjian kinerja
$queryIndikator = $db->query("SELECT * FROM `indikator_kinerja` WHERE `id_perjanjian`='$id_perjanjian' ORDER BY `id_indikator` ASC");
$data_indikator = $queryIndikator->fetchAll(PDO::FETCH_ASSOC);
$jum_indikator = count($data_indikator);

$date = date_create($data_monev['waktu_monev']);
$tanggal = date_format($date, "d");
$bulan = bulan(date_format($date, "m "));
$tahun = date_format($date, "Y ");

$waktuMonev = $tanggal . ' ' . $bulan . ' ' . $tahun;

$date = date_create($data_monev['created_at']);
$tanggal = date_format($date, "d");
$bulan = bulan(date_format($date, "m "));
$tahun = date_format($date, "Y ");

$waktuPengesahan = $tanggal . ' ' . $bulan . ' ' . $tahun;


$namaUser = str_replace(' ', '_', $data_monev['nama_pihak1']);
$namaFile = '[MONEV] PK_BLUD_' . $namaUser . '_' . $data_monev['tahun'];
// echo '<pre>';
// print_r($id_perjanjian);
// echo '</pre>';
// echo '<pre>';
// print_r($namaFile);
// echo '</pre>';
// echo '<pre>';
// print_r($data_monev);
// echo '</pre>';
// exit();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('RSUD Bandung Kiwari - Sistem Perjanjian Kinerja');
$pdf->SetTitle($namaFile);
$pdf->SetSubject('MONEV PERJANJIAN KINERJA');
$pdf->SetKeywords('MONEV, PERJANJIAN, KINERJA, PERJANJIAN KINERJA');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT + 10, PDF_MARGIN_TOP - 20, PDF_MARGIN_RIGHT + 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// ---------------------------------------------------------

// convert TTF font to TCPDF format and store it on the fonts folder
// $fontname = TCPDF_FONTS::addTTFfont('/assets/fonts/BOOKOS.ttf', 'TrueTypeUnicode', '', 96);

// use the font
// $pdf->SetFont($fontname, '', 11, '', false);
$pdf->SetFont('bookos', '', 11);


//Looping sejumlah page yang akan di cetak

// add a page
$pdf->AddPage();
$pdf->SetPrintHeader(false);
// create some HTML content
$html = '
<style>
.middel-vertical {
    vertical-align:middle;
}
</style>
<table class="table table-bordered mb-0 table-centered" style="font-size:12px">
<tbody>
    <tr>
        <td colspan="9" style="text-align: center; border:none; font-size:12px;"><b>FORMAT LAPORAN KINERJA SERTA MONITORING DAN EVALUASI INDIVIDU<br>TRIWULAN ' . $romawi[$triwulan] . ' TAHUN ' . $data_monev['tahun'] . '</b></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td width:98% colspan="3" style="border:none font-size:12px">Nama Staf yang Melaporkan Capaian Kinerja </td>
        <td colspan="5" style="border:none font-size:12px"> : ' . $data_monev['nama_pihak1'] . '</td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td width:98% colspan="3" style="border:none font-size:12px">Jabatan Pelapor Capaian Kinerja </td>
        <td colspan="5" style="border:none font-size:12px"> : ' . $data_monev['jabatan_pihak1'] . '</td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td width:98% colspan="3" style="border:none font-size:12px">Nama Pimpinan yang Melakukan Monev </td>
        <td colspan="5" style="border:none font-size:12px"> : ' . $data_monev['nama_pihak2'] . '</td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td width:98% colspan="3" style="border:none font-size:12px">Jabatan Pimpinan Pelapor </td>
        <td colspan="5" style="border:none font-size:12px"> : ' . $data_monev['jabatan_pihak2'] . '</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td width:98% colspan="8" style="border:none font-size:12px"><b>A. LAPORAN CAPAIAN KENERJA INDIVIDU DAN REALISASI ANGGARAN</b></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    </table>
    <table style="font-size:11px">
    <tr style="text-align: center; font-weight:bold; border:solid 1px #000; line-height:2">
        <td rowspan="2" style="width:5%; border: 0.6px solid black; line-height:4">No</td>
        <td colspan="6" style="width:75%; border: 0.6px solid black;">Kinerja</td>
        <td colspan="2" style="width:20%; border: 0.6px solid black;">Keterangan</td>
    </tr>
    <tr style="text-align: center; font-weight:bold;">
        <td style="width:15%; border: 0.6px solid black; line-height:2.2">Sasaran</td>
        <td style="width:20%; border: 0.6px solid black;">Indikator Kegiatan /<br>Output / Keluaran</td>
        <td style="width:10%; border: 0.6px solid black; line-height:2.2">Satuan</td>
        <td style="width:10%; border: 0.6px solid black; line-height:2.2">Target</td>
        <td style="width:10%; border: 0.6px solid black; line-height:2.2">Realisasi</td>
        <td style="width:10%; border: 0.6px solid black; line-height:2.2">%</td>
        <td style="width:10%; border: 0.6px solid black;">Faktor Pendorong</td>
        <td style="width:10%; border: 0.6px solid black;">Faktor Penghambat</td>
    </tr>
    <tr style="text-align: center; font-weight:bold; line-height:2">
        <td style="width:5%; border: 0.6px solid black; ">1</td>
        <td style="width:15%; border: 0.6px solid black;">2</td>
        <td style="width:20%; border: 0.6px solid black;">3</td>
        <td style="width:10%; border: 0.6px solid black;">4</td>
        <td style="width:10%; border: 0.6px solid black;">5</td>
        <td style="width:10%; border: 0.6px solid black;">6</td>
        <td style="width:10%; border: 0.6px solid black;">7=6/5*100%</td>
        <td style="width:10%; border: 0.6px solid black;">8</td>
        <td style="width:10%; border: 0.6px solid black;">9</td>
    </tr>';

for ($i = 0; $i < $jum_indikator; $i++) {

    $presentase = $data_indikator[$i][$indexRealisasi] / $data_indikator[$i][$indexTarget];
    $presentase = $presentase * 100;
    $html .= '<tr style="text-align: center;">
        <td style="border: 0.6px solid black;">' . ($i + 1) . '</td>';

    if ($i == 0) {
        $html .= '<td rowspan="' . $jum_indikator . '" style="text-align: left; border: 0.6px solid black;"><span class="middel-vertical">Meningkatnya Kualitas Lingkungan Sehat, Budaya Sehat, dan Mutu Pelayanan Kesehatan</span></td>';
    }
    $html .= '<td style="text-align: left; border: 0.6px solid black;">' . $data_indikator[$i]['indikator'] . '</td>
        <td style="border: 0.6px solid black;">' . $data_indikator[$i]['satuan'] . '</td>
        <td style="border: 0.6px solid black;">' . $data_indikator[$i][$indexTarget] . '</td>
        <td style="border: 0.6px solid black;">' . $data_indikator[$i][$indexRealisasi] . '</td>
        <td style="border: 0.6px solid black;">' . $presentase . ' %</td>
        <td style="border: 0.6px solid black;">-</td>
        <td style="border: 0.6px solid black;">-</td>
    </tr>';
}

$html .= '<tr style="text-align: center;">
<td style="border: 0.6px solid black;"></td>
<td colspan="5" style="border: 0.6px solid black; line-height:4">Jumlah</td>
<td style="border: 0.6px solid black; line-height:4">' . $data_monev['persentase'] . ' %</td>
<td style="border: 0.6px solid black;">-</td>
<td style="border: 0.6px solid black;">-</td>
</tr>
</tbody>
</table>';

$pdf->writeHTML($html, true, 0, true, 0);

$pdf->AddPage();
$html = '<table class="table table-bordered mb-0 table-centered">
<tbody>
    <tr>
        <td><b>B. PENILAIAN PIMPINAN</b></td>
    </tr>
    <tr>
        <td></td>
    </tr>';

$option1 = '&bull;';
$option2 = '&bull;';
$option3 = '&bull;';
$option4 = '&bull;';

if ($data_monev['persentase'] >= 100) {
    $hasil_monev = 'Sangat Berhasil (100% atau lebih)';
    $option1 = '';
} else if ($data_monev['persentase'] < 100 && $data_monev['persentase'] > 90) {
    $hasil_monev = 'Berhasil (>90%)';
    $option2 = '';
} else if ($data_monev['persentase'] >= 60 && $data_monev['persentase'] == 90) {
    $hasil_monev = 'Kurang Berhasil (60 s/d 90%)';
    $option3 = '';
} else if ($data_monev['persentase'] < 60) {
    $hasil_monev = 'Tidak Berhasil (dibawah 60%)';
    $option4 = '';
}
$html .= '<tr>
        <td style="border: 0.6px solid black;"><span style="width:100px !important"><b>PENILAIAN PIMPINAN DINILAI DARI % KINERJA (KOLOM 7)</b><br>Berdasarkan Capaian Kinerja yang diperjanjikan dan realisasi anggaran sampai dengan saat ini dapat disimpulkan bahwa dalam<br>melaksanakan tugas dan fungsi saudara termasuk dalam kriteria : ' . $hasil_monev . '</span>';
// <span style="color:red">(Pilih salah satu kriteria sesuai hasil capaian)</span><br>
// <span>' . $option1 . 'Sangat Berhasil (100% atau lebih)</span><br>
// <span>' . $option2 . ' Berhasil (>90%)</span><br>
// <span>' . $option3 . ' Kurang Berhasil (60 s/d 90%)</span><br>
// <span>' . $option4 . ' Tidak Berhasil (dibawah 60%)</span><br>
$html .= '</td>
    </tr>
    <tr>
        <td style="line-height:2"></td>
    </tr>
    <tr>
        <td><b>C. ARAHAN / SOLUSI DARI PIMPINAN</b></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="border-top: 0.6px solid black; border-right: 0.6px solid black; border-left: 0.6px solid black;">Untuk meningkatkan Capaian Kinerja dan Penyerapan anggaran, diminta agar Saudara melaksanakan hal-hal berikut :</td>
    </tr>
    <tr>
        <td style="border-bottom: 0.6px solid black; border-right: 0.6px solid black; border-left: 0.6px solid black;">
            <div style="width: 100%; border-bottom: dotted 2px #000; padding-right:10px"></div>
        </td>
    </tr>
    <tr>
        <td style="line-height:3"></td>
    </tr>
    <tr>
        <td>
            <table class="table">
                <tr>
                    <td width="50%" style="text-align:center">
                        <span style="color:#ffffff">.</span><br>
                        Telah dilakukan Monitoring dan Evaluasi<br>
                        pada tanggal ' . $waktuMonev . '
                    </td>
                    <td width="50%" style="text-align:center">
                        Bandung, ' . $waktuPengesahan . '<br></td>
                </tr>
                <tr>
                    <td width="50%" style="text-align:center;"><b>' . $data_monev['jabatan_pihak2'] . '</b></td>
                    <td width="50%" style="text-align:center;"><b>' . $data_monev['jabatan_pihak1'] . '</b></td>
                </tr>
                <tr style="color: #ffffff;">
                    <td width="50%" style="text-align:center; line-height:4">.</td>
                    <td width="50%" style="text-align:center; line-height:4">.</td>
                </tr><tr>';

if ($data_monev['golongan_pihak2']) {
    $pengesahanPihak2 = '<td width="50%" style="text-align:center"><b>' . $data_monev['nama_pihak2'] . '<br>' . $data_monev['golongan_pihak2'] . '<br>
                    NIP. ' . $data_monev['nik_pihak2'] . '</b>
                    </td>';
} else {
    $pengesahanPihak2 = '<td width="50%" style="text-align:center"><span style="color:#ffffff">.</span><br><b>' . $data_monev['nama_pihak2'] . '<br>
                    NIK. ' . $data_monev['nik_pihak2'] . '</b>
                    </td>';
}

if ($data_monev['golongan_pihak1']) {
    $pengesahanPihak1 = '<td width="50%" style="text-align:center"><br><b>' . $data_monev['nama_pihak1'] . '<br>' .
        $data_monev['golongan_pihak1'] . '<br>
                    NIP. ' . $data_monev['nik_pihak1'] . '</b></td>';
} else {
    $pengesahanPihak1 = '<td width="50%" style="text-align:center"><span style="color:#ffffff">.</span><br><b>' . $data_monev['nama_pihak1'] . '<br>
                    NIK. ' . $data_monev['nik_pihak1'] . '</b></td>';
}

$html .= $pengesahanPihak2 . $pengesahanPihak1;


$html .= ' </tr>
            </table>
        </td>
    </tr>
</tbody>
</table>';

$pdf->writeHTML($html, true, 0, true, 0);
// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output($namaFile . '.pdf', 'I');
