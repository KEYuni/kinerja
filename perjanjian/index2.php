<?php
include("../inc/pdo.conf.php");

session_start();
$_SESSION["pihak"] = 2;
$_SESSION["id_pegawai"] = 2;

$id = $_SESSION["id_pegawai"];

$currentMenu = 'approval';
$path = '../';

$queryPerjanjian = $db->query("SELECT * FROM `perjanjian_kinerja` WHERE `pihak2`='$id'");
$data_perjanjian = $queryPerjanjian->fetchAll(PDO::FETCH_ASSOC);


// echo '<pre>';
// print_r($data_perjanjian);
// echo '</pre>';
// exit();
?>
<!DOCTYPE html>
<html lang="en">

<?php
include($path . "template/header.php");
?>

<body>

    <!-- Top Bar Start -->
    <?php
    include($path . "template/topbar.php");
    ?>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php
        include($path . "template/sidenav.php");
        ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo $path ?>dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Perjanjian Kinerja</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Perjanjian Kinerja</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="d-flex" style="flex-direction: column;">
                                        <h4 class="mt-0 header-title">Daftar Perjanjian Kinerja (Approval)</h4>
                                        <p class="text-muted mb-3">List semua perjanjian kinerja yang telah dibuat.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div>
                                            <table id="datatable" class="table table-hover table-bordered mb-0 text-center dt-responsive nowrap">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th width="5%">No</th>
                                                        <th width="8%">Tahun</th>
                                                        <th width="30%">Pihak Kesatu</th>
                                                        <th width="10%">Indikator</th>
                                                        <th width="15%">Status</th>
                                                        <th width="20%">Monitoring dan Evaluasi</th>
                                                        <th width="15%">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($data_perjanjian as $i => $pk) {

                                                        $idIndikator = $pk['id_perjanjian'];
                                                        $indikator =  $db->query("SELECT id_perjanjian FROM indikator_kinerja WHERE id_perjanjian='$idIndikator'");
                                                        $jumIndikator = $indikator->rowCount();

                                                        if ($pk['status'] == 'diajukan') {
                                                            $status = '<span class="badge badge-soft-dark">Diajukan</span>';
                                                        } else if ($pk['status'] == 'disetujui') {
                                                            $status = '<span class="badge badge-soft-success">Disetujui</span>';
                                                        } else if ($pk['status'] == 'ditolak') {
                                                            $status = '<span class="badge badge-soft-danger">Ditolak</span>';
                                                        } else if ($pk['status'] == 'revisi') {
                                                            $status = '<span class="badge badge-soft-warning">Revisi</span>';
                                                        }

                                                        echo '<tr>
                                                        <td>' . ($i + 1) . '</td>
                                                        <td>' . $pk['tahun'] . '</td>
                                                        <td><b>' . $pk['nama_pihak1'] . '</b><br>(' . $pk['jabatan_pihak1'] . ')</td>
                                                        <td>' . $jumIndikator . ' Indikator</td>
                                                        <td class="text-center">' . $status . '</td>';

                                                        if ($pk['status'] == 'disetujui') {
                                                            echo '<td> <button class="btn btn-xs btn-success -perjanjian-monev" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" style="height: max-content; margin-right:0px; margin-left:auto" data-id="' . $pk['id_perjanjian'] . '">Hasil monev</button>
                                                        </td>';
                                                            echo '<td>
                                                            <a href="detail_perjanjian.php?p=' . $idIndikator . '" class="btn btn-xs btn-dark btn-block mr-2 text-white">Lihat</a>
                                                            <a href="download_perjanjian.php?p=' . $idIndikator . '" class="btn btn-xs btn-primary btn-block mr-2 text-white">Download</a>
                                                        </td>';
                                                        } else {
                                                            echo '<td></td>';
                                                            echo '<td>
                                                            <a href="detail_perjanjian.php?p=' . $idIndikator . '" class="btn btn-xs btn-dark btn-block mr-2 text-white">Lihat</a>
                                                            <a href="download_perjanjian.php?p=' . $idIndikator . '" class="btn btn-xs btn-primary btn-block mr-2 text-white">Download</a>
                                                            <a href="#" class="btn btn-xs btn-success btn-block mr-2 text-white btnHasil" data-id="' . $pk['id_perjanjian'] . '" data-status="' . $pk['status'] . '" data-revisi="' . $pk['ket_revisi'] . '" data-tolak="' . $pk['alasan_tolak'] . '" data-toggle="modal" data-animation="bounce" data-target="#modal-ajukan">Input Hasil</a>
                                                        </td>';
                                                        }

                                                        echo '</tr>';
                                                    }
                                                    ?>
                                                    <!-- <tr>
                                                        <td>1</td>
                                                        <td>2022</td>
                                                        <td><b>Arin Fitrianda</b><br>(Pelaksana SIMRS)</td>
                                                        <td>15 Indikator</td>
                                                        <td>
                                                            <span class="badge badge-soft-success">Disetujui</span>
                                                        </td>
                                                        <td> <button class="btn btn-xs btn-success" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" style="height: max-content; margin-right:0px; margin-left:auto">Hasil monev</button>
                                                        </td>
                                                        <td>
                                                            <a href="#" class="btn btn-xs btn-dark btn-block mr-2 text-white">Lihat</a>
                                                            <a href="#" class="btn btn-xs btn-primary btn-block mr-2 text-white">Download</a>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>2022</td>
                                                        <td><b>Novia Indrawati</b><br>(Pelaksana SIMRS)</td>
                                                        <td>15 Indikator</td>
                                                        <td>
                                                            <span class="badge badge-soft-dark">Belum disetujui</span>
                                                        </td>
                                                        <td>-</td>
                                                        <td>
                                                            <a href="#" class="btn btn-xs btn-dark btn-block mr-2 text-white">Lihat</a>
                                                            <a href="#" class="btn btn-xs btn-primary btn-block mr-2 text-white">Download</a>
                                                            <a href="#" class="btn btn-xs btn-success btn-block mr-2 text-white" data-toggle="modal" data-animation="bounce" data-target="#modal-ajukan">Input Hasil</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>2022</td>
                                                        <td><b>Yuniarti Musaadah</b><br>(Pelaksana SIMRS)</td>
                                                        <td>15 Indikator</td>
                                                        <td>
                                                            <span class="badge badge-soft-warning">Revisi</span>
                                                        </td>
                                                        <td>-</td>
                                                        <td>
                                                            <a href="#" class="btn btn-xs btn-dark btn-block mr-2 text-white">Lihat</a>
                                                            <a href="#" class="btn btn-xs btn-primary btn-block mr-2 text-white">Download</a>
                                                        </td>
                                                    </tr> -->
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--end table-responsive-->
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>

                <!--  Modal content for the above example -->
                <div class="modal fade bs-example-modal-lg" id="modal-hasil-monev" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0" id="myLargeModalLabel">Daftar Monitoring dan Evaluasi</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive" id="table_hasil_monev">

                                            <!-- <table class="table table-hover table-bordered mb-0">
                                                <thead class="thead-light">
                                                    <tr class="text-center">
                                                        <th width="5%">Triwulan</th>
                                                        <th width="25%">Status</th>
                                                        <th width="25%">Hasil Penilaian</th>
                                                        <th width="25%">Tanggal Monev</th>
                                                        <th width="20%" class="text-center">Dokumen</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="text-center">
                                                        <td>1</td>
                                                        <td><span class="badge badge-soft-success">Selesai</span></td>
                                                        <td><b>98%</b><br>(Berhasil)</td>
                                                        <td>29-03-2022</td>
                                                        <td>
                                                            <a href="#" class="btn btn-xs btn-primary btn-block mr-2 text-white">Lihat</a>
                                                            <a class="btn btn-xs btn-warning btn-block mr-2 text-white btn-upload-monev">Upload</a>
                                                            <button class="btn btn-xs btn-success btn-block" data-toggle="modal" data-animation="bounce" data-target="#modal-hasil-monev" style="height: max-content; margin-right:0px; margin-left:auto">Download</button>
                                                        </td>
                                                    </tr>
                                                    <tr class="text-center">
                                                        <td>2</td>
                                                        <td><span class="badge badge-soft-primary">Sudah dimonev</span></td>
                                                        <td><b>100%</b><br>(Sangat Berhasil)</td>
                                                        <td>30-06-2022</td>
                                                        <td>
                                                            <a href="detail_monev.php?p=1&t=3" class=" btn btn-xs btn-primary btn-block mr-2 text-white">Lihat</a>
                                                            <a class="btn btn-xs btn-warning btn-block mr-2 text-white btn-upload-monev">Upload</a>
                                                        </td>
                                                    </tr>
                                                    <tr class="text-center">
                                                        <td>3</td>
                                                        <td><span class="badge badge-soft-danger">Belum dimonev</span></td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td><a href="input_monev.php?p=1&t=3" class="btn btn-xs btn-secondary btn-block mr-2 text-white">Input hasil monev</a></td>
                                                    </tr>
                                                    <tr class="text-center">
                                                        <td>4</td>
                                                        <td><span class="badge badge-soft-danger">Belum dimonev</span></td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td><a href="input_monev.php" class="btn btn-xs btn-secondary btn-block mr-2 text-white">Input hasil monev</a></td>
                                                    </tr>
                                                </tbody>
                                            </table> -->
                                        </div>
                                        <!--end table-responsive-->
                                    </div>
                                    <!--end col-->
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="modal fade bs-example-modal-lg" id="modal-ajukan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0" id="myLargeModalLabel">Input Hasil Perjanjian Kinerja</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form action="hasil_perjanjian.php" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <input type="text" id="id_perjanjian" name="id_perjanjian" hidden>
                                                    <input type="text" id="hasil_perjanjian" name="hasil_perjanjian" hidden>
                                                    <h5 class="mt-0 header-title">Hasil perjanjian kinerja : </h5>
                                                    <div class="btn-group mb-4 d-block text-center" role="group" aria-label="Basic example">
                                                        <button type="button" class="btn btn-outline-success" style="width: 32%;" id="btn-setuju">Setujui</button>
                                                        <button type="button" class="btn btn-outline-warning" style="width: 32%;" id="btn-revisi"></i>Revisi</button>
                                                        <button type="button" class="btn btn-outline-danger" style="width: 32%;" id="btn-tolak">Tolak</button>
                                                    </div>
                                                    <div id="hasil-setuju" style="display: none;">
                                                        <hr>
                                                        <h5 class="mt-0 header-title">Upload file perjanjian kinerja : </h5>
                                                        <p class="text-muted mb-3">File yang diupload harus sudah ditandatangani pihak kedua.</p>
                                                        <input type="file" name="file_setuju" id="file_setuju" class="dropify" />
                                                    </div>
                                                    <div id="hasil-revisi" style="display: none;">
                                                        <hr>
                                                        <h5 class="mt-0 header-title">Keterangan Revisi : </h5>
                                                        <textarea id="ket_revisi" name="ket_revisi"></textarea>
                                                    </div>
                                                    <div id="hasil-tolak" style="display: none;">
                                                        <hr>
                                                        <h5 class="mt-0 header-title">Alasan Tolak : </h5>
                                                        <textarea id="alasan_tolak" name="alasan_tolak"></textarea>
                                                    </div>
                                                </div>
                                                <!--end card-body-->
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-right">
                                            <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 100px;">Batal</button>
                                            <button class="btn btn-sm btn-success" type="button" id="btn-hasil-perjanjian" style="width: 100px;">Simpan</button>
                                        </div>
                                        <!--end col-->
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="modal fade bs-example-modal-lg" id="modal-doc-monev" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0" id="myLargeModalLabel">Laporan Hasil Monev</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form action="upload_doc_monev.php" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="mt-0 header-title">Upload file hasil monev</h4>
                                                    <p class="text-muted mb-3">File yang diupload harus sudah ditandatangani oleh yang melakukan monitoring dan evaluasi kinerja.</p>
                                                    <input type="hidden" name="id_monev" id="id_monev" class="from-group" />
                                                    <input type="file" name="doc_monev" id="doc_monev" class="dropify" required />
                                                </div>
                                                <!--end card-body-->
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-right">
                                            <button type="button" class="btn btn-sm btn-success" id="btn-upload-monev" style="width: 100px;">Upload Hasil</button>
                                        </div>
                                        <!--end col-->
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div><!-- container -->

            <footer class="footer text-center text-sm-left">
                &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php
    include($path . "template/footer.php");
    ?>

    <script type="text/javascript">
        $kosong = '';

        $(document).ready(function() {
            if ($("#alasan_tolak").length > 0) {
                tinymce.init({
                    selector: "textarea#alasan_tolak",
                    theme: "modern",
                    height: 150,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor",
                    style_formats: [{
                            title: 'Bold text',
                            inline: 'b'
                        },
                        {
                            title: 'Red text',
                            inline: 'span',
                            styles: {
                                color: '#ff0000'
                            }
                        },
                        {
                            title: 'Red header',
                            block: 'h1',
                            styles: {
                                color: '#ff0000'
                            }
                        },
                        {
                            title: 'Example 1',
                            inline: 'span',
                            classes: 'example1'
                        },
                        {
                            title: 'Example 2',
                            inline: 'span',
                            classes: 'example2'
                        },
                        {
                            title: 'Table styles'
                        },
                        {
                            title: 'Table row 1',
                            selector: 'tr',
                            classes: 'tablerow1'
                        }
                    ]
                });
            }
            if ($("#ket_revisi").length > 0) {
                tinymce.init({
                    selector: "textarea#ket_revisi",
                    theme: "modern",
                    height: 150,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor",
                    style_formats: [{
                            title: 'Bold text',
                            inline: 'b'
                        },
                        {
                            title: 'Red text',
                            inline: 'span',
                            styles: {
                                color: '#ff0000'
                            }
                        },
                        {
                            title: 'Red header',
                            block: 'h1',
                            styles: {
                                color: '#ff0000'
                            }
                        },
                        {
                            title: 'Example 1',
                            inline: 'span',
                            classes: 'example1'
                        },
                        {
                            title: 'Example 2',
                            inline: 'span',
                            classes: 'example2'
                        },
                        {
                            title: 'Table styles'
                        },
                        {
                            title: 'Table row 1',
                            selector: 'tr',
                            classes: 'tablerow1'
                        }
                    ],
                });
            }
        });
        $(function() {
            $('.btn-monev').click(function() {
                var dataId = $(this).attr("data-id");
                $('#id_monev').val(dataId)

                console.log('dataId')
                console.log('yuni')
                console.log(dataId)

                $('#modal-hasil-monev').modal('hide')
                $('#modal-doc-monev').modal('show')

            });
        });

        function showModalUploadDoc(id) {
            console.log('aku adalah kapiten')
            $('#id_monev').val(id)

        }

        function statusMonev(nilai) {
            if (nilai >= 100) {
                status = 'Sangat Berhasil';
            } else if (nilai > 90 && nilai < 100) {
                status = 'Berhasil';
            } else if (nilai >= 60 && nilai <= 90) {
                status = 'Kurang Berhasil';
            } else if (nilai < 60) {
                status = 'Tidak Berhasil';
            }

            return status
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [day, month, year].join('-');
        }

        $(".-perjanjian-monev").click(function() {
            id = $(this).attr("data-id");
            var fd = new FormData();
            fd.append("id", id);

            var romawi = ['', 'I', 'II', 'III', 'IV']

            console.log(id)
            $.ajax({
                type: 'POST',
                url: 'ajax_data/list_monev.php',
                data: fd,
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    var response = JSON.parse(response);
                    console.log(response);
                    console.log(response['triwulan']);

                    html = '<table class="table table-hover table-bordered mb-0">' +
                        '<thead class="thead-light">' +
                        '<tr class="text-center">' +
                        '<th width="5%">Triwulan</th>' +
                        '<th width="20%">Status</th>' +
                        '<th width="25%">Hasil Penilaian</th>' +
                        '<th width="25%">Tanggal Monev</th>' +
                        '<th width="25%" class="text-center">Aksi</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>'

                    for (i = 1; i < 5; i++) {



                        html += '<tr class="text-center">' +
                            '<td>' + romawi[i] + '</td>'

                        if (response[i]['status'] == '0') {
                            html += '<td><span class="badge badge-soft-primary">Telah dimonev</span></td>' +
                                '<td><b>' + response[i]['persentase'] + '%</b><br>(' + statusMonev(response[i]['persentase']) + ')</td>' +
                                '<td>' + formatDate(response[i]['created_at']) + '</td>' +
                                '<td>' +
                                '<a href="detail_monev.php?m=' + response[i]['id_monev'] + '&t=' + i + '" class="btn btn-xs btn-primary btn-block mr-2 text-white">Lihat</a>' +
                                '<a type="button" class="btn btn-xs btn-warning btn-block mr-2 text-white btn-monev" data-dismiss="modal" data-id="' + response[i]['id_monev'] + '" data-toggle="modal" data-animation="bounce" data-target="#modal-doc-monev" onclick="showModalUploadDoc(' + response[i]['id_monev'] + ')">Upload</a>'
                        } else if (response[i]['status'] == '1') {
                            html += '<td><span class="badge badge-soft-success">Selesai</span></td>' +
                                '<td><b>' + response[i]['persentase'] + '%</b><br>(' + statusMonev(response[i]['persentase']) + ')</td>' +
                                '<td>29-03-2022</td>' +
                                '<td>' +
                                '<a href="detail_monev.php?m=' + response[i]['id_monev'] + '&t=' + i + '" class="btn btn-xs btn-primary btn-block mr-2 text-white">Lihat</a>' +
                                '<a class="btn btn-xs btn-warning btn-block mr-2 text-white btn-monev" data-dismiss="modal" data-id="' + response[i]['id_monev'] + '"data-toggle="modal" data-animation="bounce" data-target="#modal-doc-monev" onclick="showModalUploadDoc(' + response[i]['id_monev'] + ')">Upload</a>' +
                                '<a href="download_monev.php?m=' + response[i]['id_monev'] +
                                '" class="btn btn-xs btn-success btn-block mr-2 text-white">Download</a>'
                            // '<button class="btn btn-xs btn-success btn-block" style="height: max-content; margin-right:0px; margin-left:auto">Download</button>'
                        } else {
                            html += '<td><span class="badge badge-soft-info">Belum dimonev</span></td>' +
                                '<td>-</td>' +
                                '<td>-</td>' +
                                '<td>' +
                                '<a href="input_monev.php?p=' + response[i]['id_perjanjian'] + '&t=' + i + '" class="btn btn-xs btn-secondary btn-block mr-2 text-white">Input hasil monev</a>'

                        }

                        html += '</td>' +
                            '</tr>'

                    }

                    '</tbody>' +
                    '</table>'

                    const element = document.getElementById("table_hasil_monev");
                    var container = document.createElement("div");
                    container.innerHTML = html;
                    element.innerHTML = ''
                    element.appendChild(container);
                    // swal.fire(
                    //     'Deleted!',
                    //     'Your file has been deleted.',
                    //     'success'
                    // )
                }
            });
        });

        $("#btn-upload-monev").click(function() {
            id_monev = $('#id_monev').val()
            doc_monev = $('#doc_monev').prop('files')[0];

            var fd = new FormData();
            fd.append("id_monev", id_monev);
            fd.append("doc_monev", doc_monev);

            $.ajax({
                type: 'POST',
                url: 'ajax_data/upload_doc_monev.php',
                data: fd,
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    console.log(response);
                    swal.fire({
                        title: "Monev Perjanjian Kinerja",
                        text: "Dokumen Hasil Perjanjian kinerja berhasil disimpan.",
                        type: 'success',
                    }).then((result) => {
                        window.location = "index2.php";
                    })
                }
            });
        })

        $("#btn-hasil-perjanjian").click(function() {
            id_perjanjian = $('#id_perjanjian').val()
            hasil_perjanjian = $('#hasil_perjanjian').val()
            file_setuju = $('#file_setuju').prop('files')[0];
            revisi = tinymce.get('ket_revisi').getContent()
            tolak = tinymce.get('alasan_tolak').getContent()

            var fd = new FormData();
            fd.append("id_perjanjian", id_perjanjian);
            fd.append("hasil_perjanjian", hasil_perjanjian);
            fd.append("file_setuju", file_setuju);
            fd.append("revisi", revisi);
            fd.append("tolak", tolak);

            $.ajax({
                type: 'POST',
                url: 'ajax_data/hasil_perjanjian.php',
                data: fd,
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    console.log(response);
                    swal.fire({
                        title: "Hasil Perjanjian Kinerja",
                        text: "Hasil Perjanjian kinerja berhasil disimpan.",
                        type: 'success',
                    }).then((result) => {
                        window.location = "index2.php";
                    })
                }
            });
        })

        $(".btnHasil").click(function() {
            id = $(this).attr("data-id");
            status = $(this).attr("data-status");
            revisi = $(this).attr("data-revisi");
            tolak = $(this).attr("data-tolak");

            $('#id_perjanjian').val(id)


            if (status == 'revisi') {
                document.getElementById("hasil-revisi").style.display = 'block';
                document.getElementById("hasil-setuju").style.display = 'none';
                document.getElementById("hasil-tolak").style.display = 'none';
                $("#btn-revisi").addClass("active");
                $("#btn-setuju").removeClass("active");
                $("#btn-tolak").removeClass("active");

                $('#hasil_perjanjian').val('revisi')

                tinymce.get('ket_revisi').setContent(revisi);

            } else if (status == 'ditolak') {
                document.getElementById("hasil-tolak").style.display = 'block';
                document.getElementById("hasil-revisi").style.display = 'none';
                document.getElementById("hasil-setuju").style.display = 'none';
                $("#btn-tolak").addClass("active");
                $("#btn-revisi").removeClass("active");
                $("#btn-setuju").removeClass("active");

                $('#hasil_perjanjian').val('tolak')
                tinymce.get('alasan_tolak').setContent(tolak);

            }
            console.log(id)
            console.log(status)
            console.log(revisi)
        })

        $("#btn-setuju").click(function() {
            document.getElementById("hasil-setuju").style.display = 'block';
            document.getElementById("hasil-revisi").style.display = 'none';
            document.getElementById("hasil-tolak").style.display = 'none';
            $("#btn-setuju").addClass("active");
            $("#btn-revisi").removeClass("active");
            $("#btn-tolak").removeClass("active");

            $('#hasil_perjanjian').val('setuju')

            tinymce.get('ket_revisi').setContent($kosong);
            tinymce.get('alasan_tolak').setContent($kosong);
        })

        $("#btn-revisi").click(function() {
            document.getElementById("hasil-revisi").style.display = 'block';
            document.getElementById("hasil-setuju").style.display = 'none';
            document.getElementById("hasil-tolak").style.display = 'none';
            $("#btn-revisi").addClass("active");
            $("#btn-setuju").removeClass("active");
            $("#btn-tolak").removeClass("active");

            $('#hasil_perjanjian').val('revisi')
            tinymce.get('alasan_tolak').setContent($kosong);

            // $('#file_setuju').val('')
            // $('#alasan_tolak').val('')

        })

        $("#btn-tolak").click(function() {
            document.getElementById("hasil-tolak").style.display = 'block';
            document.getElementById("hasil-revisi").style.display = 'none';
            document.getElementById("hasil-setuju").style.display = 'none';
            $("#btn-tolak").addClass("active");
            $("#btn-revisi").removeClass("active");
            $("#btn-setuju").removeClass("active");

            $('#hasil_perjanjian').val('tolak')
            tinymce.get('ket_revisi').setContent($kosong);

            // $('#file_setuju').val('')
            // $('#ket_revisi').val('')

        })
    </script>
</body>

</html>