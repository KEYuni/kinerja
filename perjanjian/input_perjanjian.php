<?php
include("../inc/pdo.conf.php");
include("../api/pegawai.php");

session_start();

$currentMenu = 'perjanjian';
$path = '../';

$pegawaiAPI = getPegawai();
$dataPegawai = array(
    [
        'id_pegawai' => '2',
        'nama' => 'Iwang Suwangsih, SE',
        'jabatan' => 'PLT. Kepala Sub. Bagian Perencanaan dan Anggaran',
        'nik' => '198004282007012018',
        'golongan' => 'Penata Muda Tk.I',
    ],
    [
        'id_pegawai' => '3',
        'nama' => 'Iwan Setiawan',
        'jabatan' => 'PLT. Kepala Sub. Bagian Tata Usaha',
        'nik' => '196509291988031008',
        'golongan' => 'Penata Tk.I, III/d',
    ],
);

$id_perjanjian = isset($_GET["p"]) ? $_GET['p'] : '';
$data_perjanjian = '';
$dataIndikator = '';

if ($id_perjanjian) {
    $label = 'Edit';
    $queryPerjanjian = $db->query("SELECT * FROM `perjanjian_kinerja` WHERE `id_perjanjian`='$id_perjanjian'");
    $data_perjanjian = $queryPerjanjian->fetch(PDO::FETCH_ASSOC);

    $queryIndikator = $db->query("SELECT * FROM `indikator_kinerja` WHERE `id_perjanjian`='$id_perjanjian' ORDER BY `id_indikator` ASC");
    $dataIndikator = $queryIndikator->fetchAll(PDO::FETCH_ASSOC);

    if ($data_perjanjian['pihak1'] != $_SESSION['id_pegawai']) {
        header("location:index.php");
    }
} else {
    $label = 'Buat';
}

// echo '<pre>';
// print_r($data_perjanjian);
// echo '</pre>';
// echo '<pre>';
// print_r($_SESSION);
// echo '</pre>';
// exit();
?>
<!DOCTYPE html>
<html lang="en">

<?php
include($path . "template/header.php");
?>

<body>

    <!-- Top Bar Start -->
    <?php
    include($path . "template/topbar.php");
    ?>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php
        include($path . "template/sidenav.php");
        ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="index.php">Perjanjian Kinerja</a></li>
                                    <li class="breadcrumb-item active"><?php echo $label ?></li>
                                </ol>
                            </div>
                            <h4 class="page-title"><?php echo $label ?> Perjanjian Kinerja</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!-- end page title end breadcrumb -->
                <?php if ($data_perjanjian) {
                    if ($data_perjanjian['status'] == 'revisi') { ?>
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="card" style="border: solid 1px #dedede !important">
                                    <div class="card-header bg-warning">
                                        <h5>KETERANGAN REVISI</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <p><?php echo $data_perjanjian['ket_revisi'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } else if ($data_perjanjian['status'] == 'ditolak') { ?>
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="card" style="border: solid 1px #dedede !important">
                                    <div class="card-header bg-danger text-white">
                                        <h5>ALASAN DITOLAK</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="d-flex" style="flex-direction: column; margin-right:15px">
                                                <h6>Nama</h6>
                                                <h6>Jabatan</h6>
                                                <h6>NIK</h6>
                                            </div>
                                            <div class="d-flex" style="flex-direction: column;">
                                                <h6>: <?php echo $data_perjanjian['nama_pihak1'] ?></h6>
                                                <h6>: <?php echo $data_perjanjian['jabatan_pihak1'] ?></h6>
                                                <h6>: <?php echo $data_perjanjian['nik_pihak1'] ?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php }
                } ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">Input Indikator Kinerja</h4>
                                <p class="text-muted mb-3">Input Indikator Kinerja selama satu tahun kerja
                                </p>

                                <form class="form-parsley" action="save_perjanjian.php" method="post" id="form-perjanjian">

                                    <input type="hidden" name="id_perjanjian" id="id_perjanjian" value="<?php if ($data_perjanjian) {
                                                                                                            echo $data_perjanjian['id_perjanjian'];
                                                                                                        } ?>">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-sm-2">
                                                    <label class="col-form-label text-left">Tahun perjanjian </label>
                                                    <span class="text-danger">*</span>
                                                </div>
                                                <div class="col-sm-10">
                                                    <select class="select2 form-control" name="tahun" id="tahun">
                                                        <option value="">-- Pilih tahun perjanjian dibuat --</option>
                                                        <option <?php if ($data_perjanjian && $data_perjanjian['tahun'] == '2020') {
                                                                    echo 'selected';
                                                                } ?>>2020</option>
                                                        <option <?php if ($data_perjanjian && $data_perjanjian['tahun'] == '2021') {
                                                                    echo 'selected';
                                                                } ?>>2021</option>
                                                        <option <?php if ($data_perjanjian && $data_perjanjian['tahun'] == '2022') {
                                                                    echo 'selected';
                                                                } ?>>2022</option>
                                                        <option <?php if ($data_perjanjian && $data_perjanjian['tahun'] == '2023') {
                                                                    echo 'selected';
                                                                } ?>>2023</option>
                                                        <option <?php if ($data_perjanjian && $data_perjanjian['tahun'] == '2024') {
                                                                    echo 'selected';
                                                                } ?>>2024</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-sm-2">
                                                    <label class="col-form-label text-left">Pihak kedua perjanjian</label>
                                                    <span class="text-danger">*</span>
                                                </div>
                                                <div class="col-sm-10">
                                                    <select class="select2 form-control" id="pihak2" name="pihak2">
                                                        <option value="">-- Pilih pihak kedua perjanjian kinerja --</option>
                                                        <?php foreach ($dataPegawai as $i => $pegawai) { ?>
                                                            <option value="<?php echo $pegawai['id_pegawai'] ?>" <?php if ($data_perjanjian && $data_perjanjian['pihak2'] == $pegawai['id_pegawai']) {
                                                                                                                        echo 'selected';
                                                                                                                    } ?>><?php echo $pegawai['nama'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>

                                    <?php if ($data_perjanjian && $dataIndikator) { ?>
                                        <input type="text" id="noFormIndikator" name="noFormIndikator" value="<?php echo count($dataIndikator) ?>" hidden>
                                        <div id="divFormIndikator">
                                            <?php foreach ($dataIndikator as $i => $indikator) { ?>
                                                <div class="row px-2" id="formIndikator<?php echo $i ?>" style="margin-right: 5px;">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label>Indikator Kinerja </label>
                                                            <span class="text-danger">*</span>
                                                            <textarea required class="form-control" rows="6" id="indikator<?php echo $i ?>" name="indikator[]"><?php echo $indikator['indikator'] ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <div class="col-md-11">
                                                                <div class="row">
                                                                    <div class="col-md-12 mb-1">
                                                                        <div class="form-group">
                                                                            <label>Satuan</label>
                                                                            <span class="text-danger">*</span>
                                                                            <input type="text" class="form-control" id="satuan<?php echo $i ?>" name="satuan[]" value="<?php echo $indikator['satuan'] ?>" placeholder="Masukan satuan output dari indikator. Contoh: Laporan." required />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12 mb-1">
                                                                        <label>Target triwulan</label>
                                                                        <span class="text-danger">*</span>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text">I </span>
                                                                            </div>
                                                                            <input type="number" class="form-control" id="triwulan<?php echo $i ?>1" name="triwulan1[]" min="1" value="<?php echo $indikator['target_1'] ?>" required />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text">II</span>
                                                                            </div>
                                                                            <input type="number" class="form-control" id="triwulan<?php echo $i ?>2" name="triwulan2[]" min="1" value="<?php echo $indikator['target_2'] ?>" required />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text">III</span>
                                                                            </div>
                                                                            <input type="number" class="form-control" id="triwulan<?php echo $i ?>3" name="triwulan3[]" min="1" value="<?php echo $indikator['target_3'] ?>" required />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text">IV</span>
                                                                            </div>
                                                                            <input type="number" class="form-control" id="triwulan<?php echo $i ?>4" name="triwulan4[]" min="1" value="<?php echo $indikator['target_4'] ?>" required />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button type="button" class="btn btn-danger h-100" onclick="delFormIndokator(<?php echo $i ?>)"><i class="fas fa-trash-alt font-16"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr id="hrFormIndikator<?php echo $i ?>">
                                            <?php } ?>
                                        </div>
                                    <?php } else { ?>
                                        <input type="text" id="noFormIndikator" name="noFormIndikator" value="0" hidden>
                                        <div class="row px-2" id="formIndikator0" style="margin-right: 5px;">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Indikator Kinerja </label>
                                                    <span class="text-danger">*</span>
                                                    <textarea required class="form-control" rows="6" id="indikator0" name="indikator[]"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <div class="row">
                                                            <div class="col-md-12 mb-1">
                                                                <div class="form-group">
                                                                    <label>Satuan </label>
                                                                    <span class="text-danger">*</span>
                                                                    <input type="text" class="form-control" id="satuan0" name="satuan[]" required placeholder="Masukan satuan output dari indikator. Contoh: Laporan." />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 mb-1">
                                                                <label>Target triwulan </label>
                                                                <span class="text-danger">*</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">I </span>
                                                                    </div>
                                                                    <input type="number" class="form-control" id="triwulan01" name="triwulan1[]" min="1" required />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">II</span>
                                                                    </div>
                                                                    <input type="number" class="form-control" id="triwulan02" name="triwulan2[]" min="1" required />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">III</span>
                                                                    </div>
                                                                    <input type="number" class="form-control" id="triwulan03" name="triwulan3[]" min="1" required />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">IV</span>
                                                                    </div>
                                                                    <input type="number" class="form-control" id="triwulan04" name="triwulan4[]" min="1" required />
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Target triwulan 1</label>
                                                                    <input type="number" class="form-control" id="triwulan01" name="triwulan1[]" min="1" required />
                                                                </div>
                                                            </div> -->
                                                            <!-- <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Target triwulan 2</label>
                                                                    <input type="number" class="form-control" id="triwulan02" name="triwulan2[]" min="1" required />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Target triwulan 3</label>
                                                                    <input type="number" class="form-control" id="triwulan03" name="triwulan3[]" min="1" required />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Target triwulan 4</label>
                                                                    <input type="number" class="form-control" id="triwulan04" name="triwulan4[]" min="1" required />
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1" style="padding-right: 5px;">
                                                        <button type="button" class="btn btn-danger btn-block h-100" onclick="delFormIndokator(0)"><i class="fas fa-trash-alt font-16"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr id="hrFormIndikator0">
                                        <div id="divFormIndikator">
                                        </div>
                                    <?php } ?>

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="button" class="btn btn-sm btn-success" id="addFormIndikator">Tambah Indikator Kinerja</button>
                                        </div>
                                    </div>
                                    <!--end form-group-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group mb-0 mt-3">
                                                <a href="index.php">
                                                    <button type="button" class="btn btn-danger waves-effect m-l-5">
                                                        Batal
                                                    </button>
                                                </a>
                                                <button type="button" class="btn btn-primary waves-effect waves-light float-right" id="btn-save-perjanjian">
                                                    Simpan
                                                </button>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                    </div>
                                </form>
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div><!-- container -->

            <footer class="footer text-center text-sm-left">
                &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php
    include($path . "template/footer.php");
    ?>

    <script type="text/javascript">
        $(function() {
            //A title with a text under
            $('#btn-save-perjanjian').click(function() {

                id_perjanjian = $("#id_perjanjian").val();
                tahun = $("#tahun").val();
                pihak2 = $("#pihak2").val();

                if (id_perjanjian) {
                    aksi = 'edit';
                } else {
                    aksi = 'create';
                }

                console.log('id_perjanjian')
                console.log(id_perjanjian)

                var fd = new FormData();
                fd.append("id_perjanjian", id_perjanjian);
                fd.append("aksi", aksi);
                fd.append("tahun", tahun);

                $.ajax({
                    type: 'POST',
                    url: 'ajax_data/cek_perjanjian.php',
                    data: fd,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(response) {
                        response = JSON.parse(response)
                        console.log(response)

                        // if()
                        if (response.isExist == 1) {

                            if (aksi == 'create') {
                                swal.fire({
                                    title: "Perjanjian Kinerja",
                                    text: "Perjanjian kinerja pada tahun " + tahun +
                                        " sudah ada.",
                                    type: 'error',
                                })
                            } else if (aksi == 'edit') {}

                        } else {
                            $.ajax({
                                type: 'POST',
                                url: 'ajax_data/save_perjanjian.php',
                                data: fd,
                                contentType: false,
                                cache: false,
                                processData: false,
                                success: function(response) {
                                    // response = JSON.parse(response)
                                    console.log(response)

                                    // if()
                                    if (response.isExist == 1) {

                                        if (aksi == 'create') {
                                            swal.fire({
                                                title: "Perjanjian Kinerja",
                                                text: "Perjanjian kinerja pada tahun " + tahun +
                                                    " sudah ada.",
                                                type: 'error',
                                            })
                                        } else if (aksi == 'edit') {}

                                    } else {
                                        var indikator = [];
                                        $("textarea[name='indikator[]']").each(function() {
                                            var text = $(this).val();
                                            console.log($(this))
                                            indikator.push(text);
                                        });

                                        var satuan = $("input[name='satuan[]']")
                                            .map(function() {
                                                return $(this).val();
                                            }).get();

                                        var triwulan1 = $("input[name='triwulan1[]']")
                                            .map(function() {
                                                return $(this).val();
                                            }).get();

                                        var triwulan2 = $("input[name='triwulan2[]']")
                                            .map(function() {
                                                return $(this).val();
                                            }).get();

                                        var triwulan3 = $("input[name='triwulan3[]']")
                                            .map(function() {
                                                return $(this).val();
                                            }).get();

                                        var triwulan4 = $("input[name='triwulan4[]']")
                                            .map(function() {
                                                return $(this).val();
                                            }).get();

                                        console.log(indikator)
                                        console.log(satuan)
                                        console.log(triwulan1)
                                        console.log(triwulan2)
                                        console.log(triwulan3)
                                        console.log(triwulan4)

                                        index_error = 0;
                                        error_msg = [];

                                        if (!tahun) {
                                            error_msg[index_error] = 'Tahun perjanjian'
                                            index_error += 1
                                        }

                                        if (!pihak2) {
                                            error_msg[index_error] = ' Pihak kedua perjanjian'
                                            index_error += 1
                                        }

                                        if (!indikator) {
                                            error_msg[index_error] = ' Indikator kinerja'
                                            index_error += 1
                                        } else {
                                            cekIndikator = indikator.includes('');

                                            if (cekIndikator) {
                                                error_msg[index_error] = ' Indikator kinerja'
                                                index_error += 1
                                            }
                                            console.log(cekIndikator)
                                        }

                                        if (!satuan) {
                                            error_msg[index_error] = ' Indikator kinerja'
                                            index_error += 1
                                        } else {
                                            cekSatuan = satuan.includes('');

                                            if (cekSatuan) {
                                                error_msg[index_error] = ' Satuan indikator kinerja'
                                                index_error += 1
                                            }
                                        }

                                        if (!triwulan1) {
                                            error_msg[index_error] = ' Indikator kinerja'
                                            index_error += 1
                                        } else {
                                            cekTriwulan1 = triwulan1.includes('');

                                            if (cekTriwulan1) {
                                                error_msg[index_error] = ' Target indikator triwulan ke 1'
                                                index_error += 1
                                            }
                                        }

                                        if (!triwulan2) {
                                            error_msg[index_error] = ' Indikator kinerja'
                                            index_error += 1
                                        } else {
                                            cekTriwulan2 = triwulan2.includes('');

                                            if (cekTriwulan2) {
                                                error_msg[index_error] = ' Target indikator triwulan ke 2'
                                                index_error += 1
                                            }
                                        }

                                        if (!triwulan3) {
                                            error_msg[index_error] = ' Indikator kinerja'
                                            index_error += 1
                                        } else {
                                            cekTriwulan3 = triwulan3.includes('');

                                            if (cekTriwulan3) {
                                                error_msg[index_error] = ' Target indikator triwulan ke 3'
                                                index_error += 1
                                            }
                                        }

                                        if (!triwulan4) {
                                            error_msg[index_error] = ' Indikator kinerja'
                                            index_error += 1
                                        } else {
                                            cekTriwulan4 = triwulan4.includes('');

                                            if (cekTriwulan4) {
                                                error_msg[index_error] = ' Target indikator triwulan ke 4'
                                                index_error += 1
                                            }
                                        }

                                        if (error_msg.length > 0) {
                                            swal.fire({
                                                title: 'Perjanjian Kinerja',
                                                text: 'Mohon isi Field : ' + error_msg,
                                                type: 'warning',
                                            })
                                        } else {
                                            dataIndikator = JSON.stringify(indikator)
                                            dataSatuan = JSON.stringify(satuan)
                                            dataTriwulan1 = JSON.stringify(triwulan1)
                                            dataTriwulan2 = JSON.stringify(triwulan2)
                                            dataTriwulan3 = JSON.stringify(triwulan3)
                                            dataTriwulan4 = JSON.stringify(triwulan4)

                                            var fd = new FormData();
                                            fd.append("id_perjanjian", id_perjanjian);
                                            fd.append("aksi", aksi);
                                            fd.append("tahun", tahun);
                                            fd.append("pihak2", pihak2);
                                            fd.append("indikator", dataIndikator);
                                            fd.append("satuan", dataSatuan);
                                            fd.append("triwulan1", dataTriwulan1);
                                            fd.append("triwulan2", dataTriwulan2);
                                            fd.append("triwulan3", dataTriwulan3);
                                            fd.append("triwulan4", dataTriwulan4);

                                            $.ajax({
                                                type: 'POST',
                                                url: 'ajax_data/save_perjanjian.php',
                                                data: fd,
                                                contentType: false,
                                                cache: false,
                                                processData: false,
                                                success: function(response) {
                                                    console.log(response)
                                                    // if (response == 1) {
                                                    //     swal.fire({
                                                    //         title: "Perjanjian Kinerja",
                                                    //         text: "Perjanjian kinerja pada tahun " + tahun +
                                                    //             " sudah ada.",
                                                    //         type: 'error',
                                                    //     })
                                                    // } else {
                                                    // swal.fire({
                                                    //     title: "Perjanjian Kinerja",
                                                    //     text: "Perjanjian kinerja berhasil disimpan.",
                                                    //     type: 'success',
                                                    // }).then((result) => {
                                                    //     // window.location = "index.php";
                                                    // })
                                                    // }
                                                }
                                            });
                                        }
                                        //     swal.fire({
                                        //         title: "Perjanjian Kinerja",
                                        //         text: "Perjanjian kinerja berhasil disimpan.",
                                        //         type: 'success',
                                        //     }).then((result) => {
                                        //         // window.location = "index.php";
                                        //     })
                                    }
                                }
                            });
                        }
                    }
                });
            });

            $('#addFormIndikator').click(function() {
                no = $("#noFormIndikator").val();
                no = parseInt(no) + 1
                $("#noFormIndikator").val(no);
                console.log(no)

                html = '<div class="row px-2" id="formIndikator' + no + '" style="margin-right: 5px;">' +
                    '<div class="col-md-5">' +
                    '<div class="form-group">' +
                    '<label>Indikator Kinerja </label><span class="text-danger"> *</span>' +
                    '<textarea required class="form-control" rows="6" id="indikator' + no + '" name="indikator[]"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-7">' +
                    '<div class="row">' +
                    '<div class="col-md-11">' +
                    '<div class="row">' +
                    '<div class="col-md-12 mb-1">' +
                    '<div class="form-group">' +
                    '<label>Satuan </label><span class="text-danger"> *</span>' +
                    '<input type="text" class="form-control" id="satuan' + no + '" name="satuan[]" required placeholder="Masukan satuan output dari indikator. Contoh: Laporan." />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-12 mb-1">' +
                    '<label>Target triwulan </label><span class="text-danger"> *</span>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="input-group">' +
                    '<div class="input-group-prepend">' +
                    '<span class="input-group-text">I </span>' +
                    '</div>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '1" name="triwulan1[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="input-group">' +
                    '<div class="input-group-prepend">' +
                    '<span class="input-group-text">II</span>' +
                    '</div>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '2" name="triwulan2[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="input-group">' +
                    '<div class="input-group-prepend">' +
                    '<span class="input-group-text">III </span>' +
                    '</div>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '3" name="triwulan3[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-3">' +
                    '<div class="input-group">' +
                    '<div class="input-group-prepend">' +
                    '<span class="input-group-text">IV</span>' +
                    '</div>' +
                    '<input type="number" class="form-control" id="triwulan' + no + '4" name="triwulan4[]" min="1" required />' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-1">' +
                    '<button type="button" class="btn btn-danger h-100" onclick="delFormIndokator(' + no + ')"><i class="fas fa-trash-alt font-16"></i></button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div><hr id="hrFormIndikator' + no + '">'

                const element = document.getElementById("divFormIndikator");
                var container = document.createElement("div");
                container.innerHTML = html;
                element.appendChild(container);

            });

            $(".select2").select2({
                width: '100%'
            });
        });

        function delFormIndokator(no) {
            $("#formIndikator" + no).remove();
            $("#hrFormIndikator" + no).remove();
            // $("#val_varian" + id).remove();
            // console.log('delBadge')
            console.log(no)
        }
    </script>
</body>

</html>