<?php
require_once('../plugins/tcpdf/6_2_25/tcpdf.php');
require_once('../plugins/tcpdf/6_2_25/config/tcpdf_config.php');
include("../inc/pdo.conf.php");

function bulan($bulan)
{
    $listBulan = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

    $no = (int) $bulan;
    return $listBulan[$no];
}

class MYPDF extends TCPDF
{
    //Page header
    public function Header()
    {
    }

    // Page footer
    public function Footer()
    {
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//Get id perjanjian kinerja
$id_perjanjian = isset($_GET['p']) ? $_GET['p'] : '';

//Get detail data perjanjian kinerja
$queryPerjanjian = $db->query("SELECT * FROM `perjanjian_kinerja` WHERE `id_perjanjian`='$id_perjanjian'");
$data_perjanjian = $queryPerjanjian->fetch(PDO::FETCH_ASSOC);

//Get add indikator dari perjanjian kinerja
$queryIndikator = $db->query("SELECT * FROM `indikator_kinerja` WHERE `id_perjanjian`='$id_perjanjian' ORDER BY `id_indikator` ASC");
$data_indikator = $queryIndikator->fetchAll(PDO::FETCH_ASSOC);
$jum_indikator = count($data_indikator);

$date = date_create($data_perjanjian['created_at']);
$tanggal = date_format($date, "d");
$bulan = bulan(date_format($date, "m "));
$tahun = date_format($date, "Y ");

$waktuPengesahan = $tanggal . ' ' . $bulan . ' ' . $tahun;

$namaUser = str_replace(' ', '_', $data_perjanjian['nama_pihak1']);
$namaFile = 'PK_BLUD_' . $namaUser . '_' . $data_perjanjian['tahun'];

// echo '<pre>';
// print_r($id_perjanjian);
// echo '</pre>';
// echo '<pre>';
// print_r($data_perjanjian);
// echo '</pre>';
// echo '<pre>';
// print_r($namaFile);
// echo '</pre>';
// exit();

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('RSUD Bandung Kiwari - Sistem Perjanjian Kinerja');
$pdf->SetTitle($namaFile);
$pdf->SetSubject('PERJANJIAN KINERJA');
$pdf->SetKeywords('PERJANJIAN, KINERJA, PERJANJIAN KINERJA');

// set default header data
// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
// $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT + 10, PDF_MARGIN_TOP - 20, PDF_MARGIN_RIGHT + 10);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// ---------------------------------------------------------

// convert TTF font to TCPDF format and store it on the fonts folder
// $fontname = TCPDF_FONTS::addTTFfont('/assets/fonts/BOOKOS.ttf', 'TrueTypeUnicode', '', 96);

// use the font
// $pdf->SetFont($fontname, '', 11, '', false);
$pdf->SetFont('bookos', '', 11);

//Looping sejumlah page yang akan di cetak

// add a page
$pdf->AddPage();
$pdf->SetPrintHeader(false);
// create some HTML content
$html = '<table width="100%" style="font-size:11px; line-height:1.6">
<tr style="line-height:6">
<td colspan="2" style="text-align:center"><img src="../assets/images/Kota Bandung.png" width="100px" height="auto"></td>
</tr>
<tr>
<td colspan="2" style= "font-size:12px; text-align:center"><b>PEMERINTAH KOTA BANDUNG<br>
PERNYATAAN PERJANJIAN KINERJA<br>
DINAS KESEHATAN KOTA BANDUNG<br>
<br>
PERJANJIAN KINERJA TAHUN ' . $data_perjanjian['tahun'] . '<br>
' . strtoupper($data_perjanjian['jabatan_pihak1']) . '<br>
RUMAH SAKIT UMUM DAERAH BANDUNG KIWARI<br></b>
</td>
</tr>
<tr>
<td colspan="2" style="text-align:justify;">Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, kami yang bertanda tangan dibawah ini:</td>
</tr>
<tr>
<td colspan="2" style="text-align:center; line-height:1.5"></td>
</tr>
<tr style="line-height:2.5">
<td width="20%">Nama</td>
<td width="80%">: ' . $data_perjanjian['nama_pihak1'] . '</td>
</tr>
<tr style="line-height:2.5">
<td width="20%">Jabatan</td>
<td width="80%">: ' . $data_perjanjian['jabatan_pihak1'] . '</td>
</tr>
<tr>
<td colspan="2" style="text-align:justify; line-height:2.5">Selanjutnya disebut <b>PIHAK PERTAMA</b></td>
</tr>
<tr>
<td colspan="2" style="text-align:center; line-height:1"></td>
</tr>
<tr style="line-height:2.5">
<td width="20%">Nama</td>
<td width="80%">: ' . $data_perjanjian['nama_pihak2'] . '</td>
</tr>
<tr style="line-height:2.5">
<td width="20%">Jabatan</td>
<td width="80%">: ' . $data_perjanjian['jabatan_pihak1'] . '</td>
</tr>
<tr>
<td colspan="2" style="text-align:justify; line-height:2.5">Selaku atasan langsung pihak pertama<br>
Selanjutnya disebut <b>PIHAK KEDUA</b></td>
</tr>
<tr>
<td colspan="2" style="text-align:center; line-height:1.5"></td>
</tr>
<tr>
<td colspan="2" style="text-align:justify">Pihak pertama berjanji akan mewujudkan target kinerja tahunan sesuai lampiran perjanjian ini dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab pihak pertama.</td>
</tr>
<tr>
<td colspan="2" style="text-align:center; line-height:1.5"></td>
</tr>
<tr>
<td colspan="2" style="text-align:justify">Pihak kedua akan memberikan supervisi yang diperlukan serta akan melakukan evaluasi akuntabilitas kinerja terhadap capaian kinerja dari perjanjian ini dan mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.</td>
</tr>
<tr>
<td colspan="2" style="text-align:center; line-height:3"></td>
</tr>
<tr>
<td width="50%" style="text-align:center">
<span style="color:#ffffff">.</span><br><b>PIHAK KEDUA</b></td>
<td width="50%" style="text-align:center">
Bandung, ' . $waktuPengesahan . '<br><b>PIHAK PERTAMA</b></td>
</tr>
<tr>
<td width="50%" style="text-align:center; line-height:4"></td>
<td width="50%" style="text-align:center; line-height:4"></td>
</tr>
<tr>';

if ($data_perjanjian['golongan_pihak2']) {
    $pengesahanPihak2 = '<td width="50%" style="text-align:center"><b>' . $data_perjanjian['nama_pihak2'] . '<br>' . $data_perjanjian['golongan_pihak2'] . '<br>
    NIP. ' . $data_perjanjian['nik_pihak2'] . '</b>
    </td>';
} else {
    $pengesahanPihak2 = '<td width="50%" style="text-align:center"><span style="color:#ffffff">.</span><br><b>' . $data_perjanjian['nama_pihak2'] . '<br>
    NIK. ' . $data_perjanjian['nik_pihak2'] . '</b>
    </td>';
}

if ($data_perjanjian['golongan_pihak1']) {
    $pengesahanPihak1 = '<td width="50%" style="text-align:center"><br>' . $data_perjanjian['nama_pihak1'] . '<br>' .
        $data_perjanjian['golongan_pihak1'] . '<br>
    NIP. ' . $data_perjanjian['nik_pihak1'] . '</td>';
} else {
    $pengesahanPihak1 = '<td width="50%" style="text-align:center"><span style="color:#ffffff">.</span><br>' . $data_perjanjian['nama_pihak1'] . '<br>
    NIK. ' . $data_perjanjian['nik_pihak1'] . '</td>';
}

$html .= $pengesahanPihak2 . $pengesahanPihak1;

$html .= '</tr>
</table>';

$pdf->writeHTML($html, true, 0, true, 0);

$pdf->AddPage();
$html = '
<style>
.table-bordered{border: 0.2em solid #000}
.table-bordered td,
.table-bordered th{border: 0.2em solid #000}
.table-bordered thead td,
.table-bordered thead th{border-bottom-width:2px}

.center {
text-align:center;
}
</style>
<table style="font-size:11px; line-height:1.6">
<tr>
    <td colspan="2" style="text-align: right; font-size:12px">LAMPIRAN I/II</td>
</tr>
<tr>
    <td></td>
</tr>
<tr style="font-size:12px">
    <td colspan="2" class="center"><b>PERJANJIAN KINERJA<br>
    ' . strtoupper($data_perjanjian['jabatan_pihak1']) . '<br>
            RUMAH SAKIT UMUM DAERAH BANDUNG KIWARI<br></b>
    </td>
</tr>
<tr>
    <td width="35%"><b>&nbsp;&nbsp;PERANGKAT DAERAH</b></td>
    <td width="65%"><b>: DINAS KESEHATAN KOTA BANDUNG</b></td>
</tr>
<tr>
    <td width="35%"><b>&nbsp;&nbsp;TAHUN ANGGARAN</b></td>
    <td width="65%"><b>: ' . $data_perjanjian['tahun'] . '</b></td>
</tr>
<tr>
    <td></td>
</tr>
<tr>
    <td colspan="2">
        <table class="table table-bordered">
            <thead>
                <tr class="center" style="line-height:3">
                    <th width="7%"><b>NO.</b></th>
                    <th width="25%"><b>SASARAN STRATEGIS</b></th>
                    <th width="35%"><b>INDIKATOR KINERJA</b></th>
                    <th width="20%"><b>SATUAN</b></th>
                    <th width="13%"><b>TARGET</b></th>
                </tr>
            </thead>
            <tbody>
                <tr class="center">
                    <td>(1)</td>
                    <td>(2)</td>
                    <td>(3)</td>
                    <td>(4)</td>
                    <td>(5)</td>
                </tr>';

for ($i = 0; $i < $jum_indikator; $i++) {
    $html .= '<tr>
                    <td class="center">' . ($i + 1) . '</td>';

    if ($i == 0) {
        $html .= '<td rowspan="' . $jum_indikator . '">Meningkatnya Kualitas Lingkungan Sehat, Budaya Sehat, dan Mutu Pelayanan Kesehatan</td>';
    }

    $html .= '<td>' . $data_indikator[$i]['indikator'] . '</td>
                    <td class="center">' . $data_indikator[$i]['satuan'] . '</td>
                    <td class="center">' . ($data_indikator[$i]['target_1'] + $data_indikator[$i]['target_2'] + $data_indikator[$i]['target_3'] + $data_indikator[$i]['target_4']) . '</td>
                </tr>';
}

$html .= '</tbody>
        </table>
    </td>
</tr>
<tr style="line-height:3">
    <td></td>
</tr>
<tr>
    <td colspan="2">
        <table class="table">
            <tr>
                <td width="50%" class="center">
                    <span style="color:#ffffff">.</span><br><b>PIHAK KEDUA</b>
                </td>
                <td width="50%" class="center">
                    Bandung, ' . $waktuPengesahan . '<br><b>PIHAK PERTAMA</b></td>
            </tr>
            <tr>
                <td width="50%" style="text-align:center; line-height:4"></td>
                <td width="50%" style="text-align:center; line-height:4"></td>
            </tr>
            <tr>';

$html .= $pengesahanPihak2 . $pengesahanPihak1;

$html .= '</tr>
        </table>
    </td>
</tr>
</table>';

$pdf->writeHTML($html, true, 0, true, 0);

$pdf->AddPage();
$html = '
<style>
.table-bordered{border: 0.2em solid #000}
.table-bordered td,
.table-bordered th{border: 0.2em solid #000}
.table-bordered thead td,
.table-bordered thead th{border-bottom-width:2px}

.center {
text-align:center;
}
</style>
<table style="font-size:11px; line-height:1.6">
<tr>
    <td colspan="2" style="text-align: right;">LAMPIRAN II/II</td>
</tr>
<tr>
    <td></td>
</tr>
<tr>
    <td colspan="2" class="center"><b>PERJANJIAN KINERJA<br>
    ' . strtoupper($data_perjanjian['jabatan_pihak1']) . '<br>
            RUMAH SAKIT UMUM DAERAH BANDUNG KIWARI<br></b>
    </td>
</tr>
<tr>
    <td width="35%"><b>&nbsp;&nbsp;PERANGKAT DAERAH</b></td>
    <td width="65%"><b>: DINAS KESEHATAN KOTA BANDUNG</b></td>
</tr>
<tr>
    <td width="35%"><b>&nbsp;&nbsp;TAHUN ANGGARAN</b></td>
    <td width="65%"><b>: ' . $data_perjanjian['tahun'] . '</b></td>
</tr>
<tr>
    <td></td>
</tr>
<tr>
    <td colspan="2">
        <table class="table table-bordered">
            <thead>
                <tr class="center">
                    <th rowspan="2" width="5%"><b>NO</b></th>
                    <th rowspan="2" width="20%"><b>SASARAN STRATEGIS</b></th>
                    <th rowspan="2" width="25%"><b>INDIKATOR KINERJA</b></th>
                    <th rowspan="2" width="15%"><b>SATUAN</b></th>
                    <th rowspan="2" width="15%"><b>TARGET TAHUN ' . $data_perjanjian['tahun'] . '</b></th>
                    <th colspan="4" width="20%"><b>TARGET TRIWULAN</b></th>
                </tr>
                <tr class="center">
                    <th><b>I</b></th>
                    <th><b>II</b></th>
                    <th><b>III</b></th>
                    <th><b>IV</b></th>
                </tr>
            </thead>
            <tbody>
                <tr class="center">
                    <td>(1)</td>
                    <td>(2)</td>
                    <td>(3)</td>
                    <td>(4)</td>
                    <td>(5)</td>
                    <td colspan="4">(6)</td>
                </tr>';
for ($i = 0; $i < $jum_indikator; $i++) {
    $html .= '<tr>
            <td class="center">' . ($i + 1) . '</td>';

    if ($i == 0) {
        $html .= '<td rowspan="' . $jum_indikator . '">Meningkatnya Kualitas Lingkungan Sehat, Budaya Sehat, dan Mutu Pelayanan Kesehatan</td>';
    }

    $html .= '<td>' . $data_indikator[$i]['indikator'] . '</td>
                    <td class="center">' . $data_indikator[$i]['satuan'] . '</td>
                    <td class="center">' . ($data_indikator[$i]['target_1'] + $data_indikator[$i]['target_2'] + $data_indikator[$i]['target_3'] + $data_indikator[$i]['target_4']) . '</td>
                    <td class="center">' . $data_indikator[$i]['target_1'] . '</td>
                    <td class="center">' . $data_indikator[$i]['target_2'] . '</td>
                    <td class="center">' . $data_indikator[$i]['target_3'] . '</td>
                    <td class="center">' . $data_indikator[$i]['target_4'] . '</td>
                </tr>';
}

$html .= '</tbody>
        </table>
    </td>
</tr>
<tr style="line-height:3">
    <td></td>
</tr>
<tr>
    <td colspan="2">
        <table class="table">
            <tr>
                <td width="50%" style="text-align:center">
                    <span style="color:#ffffff">.</span><br><b>PIHAK KEDUA</b>
                </td>
                <td width="50%" style="text-align:center">
                    Bandung, ' . $waktuPengesahan . '<br><b>PIHAK PERTAMA</b></td>
            </tr>
            <tr>
                <td width="50%" style="text-align:center; line-height:4"></td>
                <td width="50%" style="text-align:center; line-height:4"></td>
            </tr>
            <tr>';

$html .= $pengesahanPihak2 . $pengesahanPihak1;

$html .= '</tr>
        </table>
    </td>
</tr>
</table>';

$pdf->writeHTML($html, true, 0, true, 0);
// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output($namaFile . '.pdf', 'I');
