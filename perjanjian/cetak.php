<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Metrica - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A premium admin dashboard template by Mannatthemes" name="description" />
    <meta content="Mannatthemes" name="author" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="../assets/images/favicon.ico">

    <!-- DataTables -->
    <link href="../assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="../assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/metisMenu.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>

    <div class="page-wrapper">

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Metrica</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Others</a></li>
                                    <li class="breadcrumb-item active">Datatable</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Datatable</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table class="table table-bordered mb-0 table-centered">
                                        <tbody>
                                            <tr>
                                                <td colspan="9" style="text-align: center; border:none"><b>FORMAT LAPORAN KINERJA SERTA MONITORING DAN EVALUASI INDIVIDU<br>TRIWULAN I TAHUN 2022</b></td>
                                            </tr>
                                            <tr>
                                                <td colspan="9" style="border:none"><b>A. LAPORAN CAPAIAN KENERJA INDIVIDU DAN REALISASI ANGGARAN</b></td>
                                            </tr>
                                            <tr style="text-align: center; font-weight:bold">
                                                <td rowspan="2">No</td>
                                                <td colspan="6">Kinerja</td>
                                                <td colspan="2">Keterangan</td>
                                            </tr>
                                            <tr style="text-align: center; font-weight:bold">
                                                <td>Sasaran</td>
                                                <td>Indikator Kegiatan /<br>Output / Keluaran</td>
                                                <td>Satuan</td>
                                                <td>Target</td>
                                                <td>Realisasi</td>
                                                <td>%</td>
                                                <td>Faktor Pendorong</td>
                                                <td>Faktor Penghambat</td>
                                            </tr>
                                            <tr style="text-align: center; font-weight:bold">
                                                <td>1</td>
                                                <td>2</td>
                                                <td>3</td>
                                                <td>4</td>
                                                <td>5</td>
                                                <td>6</td>
                                                <td>7=6/5*100%</td>
                                                <td>8</td>
                                                <td>9</td>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <td>1</td>
                                                <td rowspan="3" style="text-align: left; width:200px">Meningkatnya Kualitas Lingkungan Sehat, Budaya Sehat, dan Mutu Pelayanan Kesehatan</td>
                                                <td style="text-align: left;">Membuat laporan operasi komputer</td>
                                                <td>Laporan</td>
                                                <td>2</td>
                                                <td></td>
                                                <td></td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <td>2</td>
                                                <td style="text-align: left;">Membuat dokumentasi file yang tersimpan dalam media komputer</td>
                                                <td>Dokumen</td>
                                                <td>2</td>
                                                <td></td>
                                                <td></td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <td>3</td>
                                                <td style="text-align: left;">Melakukan pemasangan peralatan sistem komputer / sistem jaringan komputer</td>
                                                <td>Sistem</td>
                                                <td>2</td>
                                                <td></td>
                                                <td></td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <td></td>
                                                <td colspan="5">Jumlah</td>
                                                <td>Rata-rata seluruh % capaian</td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered mb-0 table-centered">
                                        <tbody>
                                            <tr>
                                                <td style="border:none"><b>B. PENILAIAN PIMPINAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i><b>PENILAIAN PIMPINAN DINILAI DARI % KINERJA (KOLOM 7)</b></i><br>
                                                    Berdasarkan Capaian Kinerja yang diperjanjikan dan realisasi anggaran sampai dengan saat ini dapat disimpulkan bahwa dalam melaksanakan tugas dan fungsi saudara termasuk dalam kriteria : Sangat Berhasil (100% atau lebih)
                                                    <span style="color:red">(Pilih salah satu kriteria sesuai hasil capaian)</span><br>
                                                    <span>Sangat Berhasil (100% atau lebih)</span><br>
                                                    <ul>
                                                        <li>
                                                            <span>Berhasil</span>
                                                            <span>(>90%)</span>
                                                        </li>
                                                        <li>
                                                            <span>Kurang Berhasil</span>
                                                            <span>(60 s/d 90%)</span>
                                                        </li>
                                                        <li>
                                                            <span>Tidak Berhasil</span>
                                                            <span>(dibawah 60%)</span>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border:none"><b>C. ARAHAN / SOLUSI DARI PIMPINAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Untuk meningkatkan Capaian Kinerja dan Penyerapan anggaran, diminta agar Saudara melaksanakan hal-hal berikut :
                                                    <span style="color:red">(harus diisi)</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 100%; border-bottom: dotted 2px #000"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="table">
                                                        <tr>
                                                            <td width="50%" style="text-align:center">
                                                                <span style="color:#ffffff">.</span><br>
                                                                Telah dilakukan Monitoring dan Evaluasi<br>
                                                                pada tanggal 31 Maret 2022
                                                            </td>
                                                            <td width="50%" style="text-align:center">
                                                                Bandung, 02 Februari 2022<br></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%" style="text-align:center;"><b>PLT. Kepala Sub. Bagian Perencanaan dan Anggaran</b></td>
                                                            <td width="50%" style="text-align:center;"><b>Pelaksana SIMRS</b></td>
                                                        </tr>
                                                        <tr style="color: #ffffff;">
                                                            <td width="50%" style="text-align:center; line-height:2">.</td>
                                                            <td width="50%" style="text-align:center; line-height:2">.</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%" style="text-align:center">
                                                                <b>Iwang Suwangsih, SE<br>Penata Muda Tk.I<br>
                                                                    NIP. 198004282007012018</b>
                                                            </td>
                                                            <td width="50%" style="text-align:center">
                                                                <span style="color:#ffffff">.</span><br><b> Yuniarti Musaadah, S.Kom<br>
                                                                    NIK. 19970622202202693</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!--end /table-->
                                </div>
                                <!--end /tableresponsive-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                        <!--end card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <!--end row-->
            </div><!-- container -->

            <footer class="footer text-center text-sm-left">
                &copy; 2019 Metrica <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Mannatthemes</span>
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/js/metisMenu.min.js"></script>
    <script src="../assets/js/waves.min.js"></script>
    <script src="../assets/js/jquery.slimscroll.min.js"></script>

    <!-- Required datatable js -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="../assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="../assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="../assets/plugins/datatables/jszip.min.js"></script>
    <script src="../assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="../assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="../assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="../assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="../assets/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="../assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="../assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
    <script src="../assets/pages/jquery.datatable.init.js"></script>


    <!-- App js -->
    <script src="../assets/js/app.js"></script>

</body>

</html>