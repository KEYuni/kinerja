<head>
    <meta charset="utf-8" />
    <title>Sistem Perjanjian Kinerja</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A premium admin dashboard template by Mannatthemes" name="description" />
    <meta content="Mannatthemes" name="author" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo $path ?>assets/images/favicon.ico">
    <link href="<?php echo $path ?>assets/plugins/dropify/css/dropify.min.css" rel="stylesheet">

    <!-- DataTables -->
    <link href="<?php echo $path ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="<?php echo $path ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo $path ?>assets/plugins/morris/morris.css">

    <!-- Sweet Alert -->
    <link href="<?php echo $path ?>assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $path ?>assets/plugins/animate/animate.css" rel="stylesheet" type="text/css">

    <!-- App css -->
    <link href="<?php echo $path ?>assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path ?>assets/css/metisMenu.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path ?>assets/css/style.css" rel="stylesheet" type="text/css" />

    <style>
        .table .thead-light th {
            vertical-align: middle;
            text-align: center;
        }
    </style>
</head>