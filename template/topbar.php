<?php
// include("../inc/pdo.conf.php");

// session_start();
$nama_user = $_SESSION['nama'];
$pihak = $_SESSION['pihak'];

if ($pihak == 1) {
    $labelPihak = 'Ubah sebagai pihak ke 2';
} else {
    $labelPihak = 'Ubah sebagai pihak ke 1';
}

?>

<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <a href="<?php echo $path ?>dashboard.php" class="logo">
            <span>
                <img src="<?php echo $path ?>assets/images/logo-sm.png" alt="logo-small" class="logo-sm">
            </span>
            <span>
                <img src="<?php echo $path ?>assets/images/logo.png" alt="logo-large" class="logo-lg">
                <img src="<?php echo $path ?>assets/images/logo-dark.png" alt="logo-large" class="logo-lg logo-dark">
            </span>
        </a>
    </div>
    <!--end logo-->
    <!-- Navbar -->
    <nav class="navbar-custom">
        <ul class="list-unstyled topbar-nav float-right mb-0">
            <li class="dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="<?php echo $path ?>assets/images/users/user-4.jpg" alt="profile-user" class="rounded-circle" />
                    <span class="ml-1 nav-user-name hidden-sm"><?php echo $nama_user ?> </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- <a class="dropdown-item" href="#"><i class="dripicons-user text-muted mr-2"></i> Profile</a>
                    <a class="dropdown-item" href="<?php echo $path ?>ubahPihak.php"><i class="dripicons-retweet text-muted mr-2"></i> <?php echo $labelPihak ?></a>
                    <a class="dropdown-item" href="#"><i class="dripicons-wallet text-muted mr-2"></i> My Wallet</a>
                    <a class="dropdown-item" href="#"><i class="dripicons-gear text-muted mr-2"></i> Settings</a>
                    <a class="dropdown-item" href="#"><i class="dripicons-lock text-muted mr-2"></i> Lock screen</a>
                    <div class="dropdown-divider"></div> -->
                    <a class="dropdown-item" href="<?php echo $path ?>logout.php"><i class="dripicons-exit text-muted mr-2"></i> Logout</a>
                </div>
            </li>
            <!-- <li class="hidden-sm">
                <a href="<?php echo $path ?>logout.php" class="nav-link dropdown-toggle waves-effect waves-light" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="dripicons-exit text-muted mr-2" style="font-size: 15px;"></i>
                </a>
            </li> -->
        </ul>
        <!--end topbar-nav-->

        <ul class="list-unstyled topbar-nav mb-0">
            <li>
                <button class="button-menu-mobile nav-link waves-effect waves-light">
                    <i class="dripicons-menu nav-icon"></i>
                </button>
            </li>
        </ul>
    </nav>
    <!-- end navbar-->
</div>
<!-- Top Bar End -->