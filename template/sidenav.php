<!-- Left Sidenav -->
<div class="left-sidenav">
    <ul class="metismenu left-sidenav-menu">
        <li <?php if ($currentMenu == 'dashboard') {
                echo 'class="mm-active"';
            } ?>>
            <a href="<?php echo $path ?>dashboard.php" <?php if ($currentMenu == 'dashboard') {
                                                            echo 'class="active"';
                                                        } ?>><i class="ti-bar-chart active"></i><span>Dashboard</span><span class="menu-arrow"></i></span></a>
        </li>

        <li <?php if ($currentMenu == 'perjanjian') {
                echo 'class="mm-active"';
            } ?>>
            <a href="<?php echo $path ?>perjanjian/index.php" <?php if ($currentMenu == 'perjanjian') {
                                                                    echo 'class="active"';
                                                                } ?>><i class="ti-server"></i><span>Perjanjian Kinerja</span><span class="menu-arrow"></span></a>
        </li>
        <li <?php if ($currentMenu == 'approval') {
                echo 'class="mm-active"';
            } ?>>
            <a href="<?php echo $path ?>perjanjian/index2.php" <?php if ($currentMenu == 'approval') {
                                                                    echo 'class="active"';
                                                                } ?>><i class="ti-server"></i><span>Approval Perjanjian</span><span class="menu-arrow"></span></a>
        </li>
    </ul>
</div>
<!-- end left-sidenav-->