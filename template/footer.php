<!-- jQuery  -->
<script src="<?php echo $path ?>assets/js/jquery.min.js"></script>
<script src="<?php echo $path ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo $path ?>assets/js/metisMenu.min.js"></script>
<script src="<?php echo $path ?>assets/js/waves.min.js"></script>
<script src="<?php echo $path ?>assets/js/jquery.slimscroll.min.js"></script>

<!-- Parsley js -->
<script src="<?php echo $path ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="<?php echo $path ?>assets/pages/jquery.validation.init.js"></script>

<script src="<?php echo $path ?>assets/js/jquery.core.js"></script>

<!-- Sweet-Alert  -->
<script src="<?php echo $path ?>assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="<?php echo $path ?>assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Required datatable js -->
<script src="<?php echo $path ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="<?php echo $path ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/jszip.min.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/buttons.print.min.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="<?php echo $path ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo $path ?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
<script src="<?php echo $path ?>assets/pages/jquery.datatable.init.js"></script>

<script src="<?php echo $path ?>assets/plugins/dropify/js/dropify.min.js"></script>
<script src="<?php echo $path ?>assets/pages/jquery.form-upload.init.js"></script>


<!--Wysiwig js-->
<script src="<?php echo $path ?>assets/plugins/tinymce/tinymce.min.js"></script>
<script src="<?php echo $path ?>assets/pages/jquery.form-editor.init.js"></script>

<!-- App js -->
<script src="<?php echo $path ?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo $path ?>assets/js/app.js"></script>